package UI_HBI.FOREIGN_EMPLOYEE;

import java.io.IOException;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_ForeignEmployee.TC0_Obj_FE_Common;
import HBI_ForeignEmployee.TC1_Obj_FE_LandingPage;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;



//CHECKS ALL THE 8 TABS ARE PRESENT AND UPON CLICKING THE NUMBER OF RECORDS IN BOTH PAGER AND TAB IS SAME
@Listeners(Allure_Listener.class) 
public class TC1_HBI_FE_LandingPage extends MainBaseFeature{
	
	TC0_Obj_FE_Common FELoginObj;
    ReadExcel takeData = new ReadExcel();
    TC1_Obj_FE_LandingPage FEVerify;
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
    	driver.get(takeData.readExcelFile("testData",1,1));
    	
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
        FELoginObj = new TC0_Obj_FE_Common();
        FELoginObj.clickLoginRegisterationButton();
       
    }

 
    @Test(priority = 4)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user selects Foreign Employer and clicks on Login button") 
    @Story("User able to find and select Foreign Employer radio button")
    public void user_selects_Foreign_Employer_and_clicks_on_Login_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_selects_Foreign_Employer_and_clicks_on_Login_button");
        FELoginObj.selectUserTypeAsFE();
        FELoginObj.clickOnUserTypePopupLogin();
        
    }

    @Test(priority = 5)
    @Severity(SeverityLevel.NORMAL)
	@Description("user enters Healthcare gmail com and Healthcare") 
    @Story("User enters username and password")
    public void user_enters_Healthcare_gmail_com_and_Healthcare() throws InterruptedException, IOException {
    	test = extent.createTest("user_enters_Healthcare_gmail_com_and_Healthcare");
            //FELoginObj.setEmailID(readPropertyFile("userEmail"));
    	    FELoginObj.setEmailID(takeData.readExcelFile("testData",4,1));
 
            //FELoginObj.setPassword(readPropertyFile("password"));
    	    FELoginObj.setPassword(takeData.readExcelFile("testData",5,1));
            
            FELoginObj.clickOnLoginDashboard();   
            
            try{
	            boolean withoutEmailID = FELoginObj.proceedWithoutEmailID();
	            Assert.assertEquals(withoutEmailID, false,"Cannot proceed without entering emailID");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	
	            boolean withoutPassw = FELoginObj.proceedWithoutPassword();
	            Assert.assertEquals(withoutPassw, false,"Cannot proceed without entering Password");
            }
            catch(Exception e){
            	
            }
            
            try {
	            boolean withoutEmailPassw = FELoginObj.invalidUserIDPassword();
	            Assert.assertEquals(withoutEmailPassw, false,"Invalid userID or Password");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	boolean validEmailID = FELoginObj.checkForValidUserID();
            	Assert.assertEquals(validEmailID, false,"Entered invalid UserID");
            }
            
            catch(Exception e){
            	
            }
                                
    }

    @Test(priority = 6)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_Login") 
    @Story("User able to click on Login button")
    public void click_on_Login() throws InterruptedException {
    	
    	test = extent.createTest("click_on_Login");
        System.out.println("Able to click on Login button");       
       
    }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Verify_dashboard") 
    @Story("User able to see dashboard")
    public void verifyDashboard() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Verify_dashboard");
    	FEVerify = new TC1_Obj_FE_LandingPage();
    	boolean dashBoardVisibleAct = FEVerify.dashBoardVisible();
    	boolean dashBoardVisibleExpt = true;
        Assert.assertEquals(dashBoardVisibleAct, dashBoardVisibleExpt);       
       
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_Initiated") 
    @Story("User clicks on initiated tab and chedcks the number of records")
    public void verifyInitiatedtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_Initiated");
    	String initiatedTab = FEVerify.TabDisplayed(2);
    	System.out.println(initiatedTab);
    	Assert.assertEquals(initiatedTab, "Initiated");
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_Initiated") 
    @Story("User checks initiated in record")
    public void verifyInitiatedtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_Initiated");
    	FEVerify.clickontab(3);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(2));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_Initiated_rec_count") 
    @Story("User checks whether the number in initiated tab is same as the number of record displayed")
    public void verifyInitiatedtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_Initiated_rec_count");
    	String InitiatedValInTab = FEVerify.Numberdashboard(3);
    	System.out.println(InitiatedValInTab);
    	
    	String initiatedListCount = FEVerify.ListNumber();	
    	String initiatedCount = initiatedListCount.substring(initiatedListCount.lastIndexOf(" ")+1, initiatedListCount.length());
    	System.out.println(initiatedCount);
    	Assert.assertEquals(InitiatedValInTab, initiatedCount);
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_EmployerConnected") 
    @Story("User clicks on EmployerConnected tab and chedcks the number of records")
    public void verifyEmployerConnectedtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_EmployerConnected");
    	String EmployerConnectedTab = FEVerify.TabDisplayed(5);
    	System.out.println(EmployerConnectedTab);
    	Assert.assertEquals(EmployerConnectedTab, "Employer connected");
    }
    
    @Test(priority = 12)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_Employer connected") 
    @Story("User checks Employer connected in record")
    public void verifyEmployerConnectedtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_EmployerConnected");
    	FEVerify.clickontab(6);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(5));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_EmployerConnected_rec_count") 
    @Story("User checks whether the number in EmployerConnected tab is same as the number of record displayed")
    public void verifyEmployerConnectedtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_EmployerConnected_rec_count");
    	String EmployerConnectedValInTab = FEVerify.Numberdashboard(6);
    	System.out.println(EmployerConnectedValInTab);
    	
    	String EmployerConnectedListCount = FEVerify.ListNumber();	
    	String EmployerConnectedCount = EmployerConnectedListCount.substring(EmployerConnectedListCount.lastIndexOf(" ")+1, EmployerConnectedListCount.length());
    	System.out.println(EmployerConnectedCount);
    	Assert.assertEquals(EmployerConnectedValInTab, EmployerConnectedCount);
    }
    
    @Test(priority = 14)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_Verified") 
    @Story("User clicks on Verified tab and chedcks the number of records")
    public void verifyVerifiedtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_Verified");
    	String VerifiedTab = FEVerify.TabDisplayed(7);
    	System.out.println(VerifiedTab);
    	Assert.assertEquals(VerifiedTab, "Verified");
    }
    
    @Test(priority = 15)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_Verified") 
    @Story("User checks Verified in record")
    public void verifyVerifiedtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_Verified");
    	FEVerify.clickontab(8);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(7));
	    	//Assert.assertEquals(statusinrecdis, 45);
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 16)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_Verified_rec_count") 
    @Story("User checks whether the number in Verified tab is same as the number of record displayed")
    public void verifyVerifiedtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_Verified_rec_count");
    	String VerifiedValInTab = FEVerify.Numberdashboard(8);
    	System.out.println(VerifiedValInTab);
    	
    	String VerifiedListCount = FEVerify.ListNumber();	
    	String VerifiedCount = VerifiedListCount.substring(VerifiedListCount.lastIndexOf(" ")+1, VerifiedListCount.length());
    	System.out.println(VerifiedCount);
    	Assert.assertEquals(VerifiedValInTab, VerifiedCount);
    }
    
    @Test(priority = 17)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_PreDepartureOrientation") 
    @Story("User clicks on PreDepartureOrientation tab and chedcks the number of records")
    public void verifyPreDepartureOrientationtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_PreDepartureOrientation");
    	String PreDepartureOrientationTab = FEVerify.TabDisplayed(9);
    	System.out.println(PreDepartureOrientationTab);
    	Assert.assertEquals(PreDepartureOrientationTab, "Pre-departure orientation");
    }
    
    @Test(priority = 18)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_PreDepartureOrientation") 
    @Story("User checks PreDepartureOrientation in record")
    public void verifyPreDepartureOrientationtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_PreDepartureOrientation");
    	FEVerify.clickontab(10);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(9));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 19)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_PreDepartureOrientation_rec_count") 
    @Story("User checks whether the number in PreDepartureOrientation tab is same as the number of record displayed")
    public void verifyPreDepartureOrientationtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_PreDepartureOrientation_rec_count");
    	String PreDepartureOrientationValInTab = FEVerify.Numberdashboard(10);
    	System.out.println(PreDepartureOrientationValInTab);
    	
    	String PreDepartureOrientationListCount = FEVerify.ListNumber();	
    	String PreDepartureOrientationCount = PreDepartureOrientationListCount.substring(PreDepartureOrientationListCount.lastIndexOf(" ")+1, PreDepartureOrientationListCount.length());
    	System.out.println(PreDepartureOrientationCount);
    	Assert.assertEquals(PreDepartureOrientationValInTab, PreDepartureOrientationCount);
    }
    
    @Test(priority = 20)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_VisaorWorkPermitProcessing") 
    @Story("User clicks on VisaorWorkPermitProcessing tab and chedcks the number of records")
    public void verifyVisaorWorkPermitProcessingtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_VisaorWorkPermitProcessing");
    	String VisaorWorkPermitProcessingTab = FEVerify.TabDisplayed(11);
    	System.out.println(VisaorWorkPermitProcessingTab);
    	Assert.assertEquals(VisaorWorkPermitProcessingTab, "Visa or Work Permit processing");
    }
    
    @Test(priority = 21)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_VisaorWorkPermitProcessing") 
    @Story("User checks VisaorWorkPermitProcessing in record")
    public void verifyVisaorWorkPermitProcessingtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_VisaorWorkPermitProcessing");
    	FEVerify.clickontab(12);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(11));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 22)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_VisaorWorkPermitProcessing_rec_count") 
    @Story("User checks whether the number in VisaorWorkPermitProcessing tab is same as the number of record displayed")
    public void verifyVisaorWorkPermitProcessingtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_VisaorWorkPermitProcessing_rec_count");
    	String VisaorWorkPermitProcessingValInTab = FEVerify.Numberdashboard(12);
    	System.out.println(VisaorWorkPermitProcessingValInTab);
    	
    	String VisaorWorkPermitProcessingListCount = FEVerify.ListNumber();	
    	String VisaorWorkPermitProcessingCount = VisaorWorkPermitProcessingListCount.substring(VisaorWorkPermitProcessingListCount.lastIndexOf(" ")+1, VisaorWorkPermitProcessingListCount.length());
    	System.out.println(VisaorWorkPermitProcessingCount);
    	Assert.assertEquals(VisaorWorkPermitProcessingValInTab, VisaorWorkPermitProcessingCount);
    }
    
    @Test(priority = 23)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_ReadyForDeparture") 
    @Story("User clicks on ReadyForDeparture tab and checks the number of records")
    public void verifyReadyForDeparturetabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_ReadyForDeparture");
    	String ReadyForDepartureTab = FEVerify.TabDisplayed(13);
    	System.out.println(ReadyForDepartureTab);
    	Assert.assertEquals(ReadyForDepartureTab, "Ready for departure");
    	//Assert.assertEquals(ReadyForDepartureTab, "Ready fhbbjbor departure");
    }
    
    @Test(priority = 24)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_ReadyForDeparture") 
    @Story("User checks ReadyForDeparture in record")
    public void verifyReadyForDeparturetabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_ReadyForDeparture");
    	FEVerify.clickontab(14);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(13));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 25)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_ReadyForDeparture_rec_count") 
    @Story("User checks whether the number in ReadyForDeparture tab is same as the number of record displayed")
    public void verifyReadyForDeparturetabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_ReadyForDeparture_rec_count");
    	String ReadyForDepartureValInTab = FEVerify.Numberdashboard(14);
    	System.out.println(ReadyForDepartureValInTab);
    	
    	String ReadyForDepartureListCount = FEVerify.ListNumber();	
    	String ReadyForDepartureCount = ReadyForDepartureListCount.substring(ReadyForDepartureListCount.lastIndexOf(" ")+1, ReadyForDepartureListCount.length());
    	System.out.println(ReadyForDepartureCount);
    	Assert.assertEquals(ReadyForDepartureValInTab, ReadyForDepartureCount);
    	
    }
    
    @Test(priority = 26)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_EmploymentStarted") 
    @Story("User clicks on EmploymentStarted tab and chedcks the number of records")
    public void verifyEmploymentStartedtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_Initiated");
    	String EmploymentStartedTab = FEVerify.TabDisplayed(15);
    	System.out.println(EmploymentStartedTab);
    	Assert.assertEquals(EmploymentStartedTab, "Employment started");
    }
    
    @Test(priority = 27)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_EmploymentStarted") 
    @Story("User checks EmploymentStarted in record")
    public void verifyEmploymentStartedtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_EmploymentStarted");
    	FEVerify.clickontab(16);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, FEVerify.TabDisplayed(15));
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    
    @Test(priority = 28)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_EmploymentStarted_rec_count") 
    @Story("User checks whether the number in EmploymentStarted tab is same as the number of record displayed")
    public void verifyEmploymentStartedtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_EmploymentStarted_rec_count");
    	String EmploymentStartedValInTab = FEVerify.Numberdashboard(16);
    	System.out.println(EmploymentStartedValInTab);
    	
    	String EmploymentStartedListCount = FEVerify.ListNumber();	
    	String EmploymentStartedCount = EmploymentStartedListCount.substring(EmploymentStartedListCount.lastIndexOf(" ")+1, EmploymentStartedListCount.length());
    	System.out.println(EmploymentStartedCount);
    	Assert.assertEquals(EmploymentStartedValInTab, EmploymentStartedCount);
    }
    
    @Test(priority = 29)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_WithdrawnRejected") 
    @Story("User clicks on WithdrawnRejected tab and chedcks the number of records")
    public void verifyWithdrawnRejectedtabCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("click_on_WithdrawnRejected");
    	String WithdrawnRejectedTab = FEVerify.TabDisplayed(17);
    	System.out.println(WithdrawnRejectedTab);
    	Assert.assertEquals(WithdrawnRejectedTab, "Withdrawn / Rejected");
    }
    /////////////////////////////////////
    @Test(priority = 30)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_record_WithdrawnRejected") 
    @Story("User checks WithdrawnRejected in record")
    public void verifyWithdrawnRejectedtabRecStatus() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_record_status_WithdrawnRejected");
    	FEVerify.clickontab(18);
    	
    	System.out.println(FEVerify.noResultFound());
    	
    	try {
        	
	    	System.out.println("---->");
	    	String statusinrecdis = FEVerify.statusInRecDisplayed();
	    	System.out.println(statusinrecdis+"---->");
	    	Assert.assertEquals(statusinrecdis, "Withdrawn", "Rejected");
    	
    	}    	
    	catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}	
    }
    ///////////////////////////////////////////
    @Test(priority = 31)
    @Severity(SeverityLevel.CRITICAL)
	@Description("check_WithdrawnRejected_rec_count") 
    @Story("User checks whether the number in WithdrawnRejected tab is same as the number of record displayed")
    public void verifyWithdrawnRejectedtabTabAndRecDispCount() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("check_WithdrawnRejected_rec_count");
    	String WithdrawnRejectedValInTab = FEVerify.Numberdashboard(18);
    	System.out.println(WithdrawnRejectedValInTab);
    	
    	String WithdrawnRejectedListCount = FEVerify.ListNumber();	
    	String WithdrawnRejectedCount = WithdrawnRejectedListCount.substring(WithdrawnRejectedListCount.lastIndexOf(" ")+1, WithdrawnRejectedListCount.length());
    	System.out.println(WithdrawnRejectedCount);
    	Assert.assertEquals(WithdrawnRejectedValInTab, WithdrawnRejectedCount);
    }
    
   
    
}
