package UI_HBI.FOREIGN_EMPLOYEE;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_ForeignEmployee.TC0_Obj_FE_Common;
import HBI_ForeignEmployee.TC2_Obj_FE_MyProfile;
import HBI_ForeignEmployee.TC3_Obj_FE_HealthcareProfessional;
import HBI_ForeignEmployee.TC4_Obj_FE_Registration1;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners(Allure_Listener.class) 
public class TC4_HBI_FE_Registration1 extends MainBaseFeature{
	
	TC0_Obj_FE_Common FELoginObj;
    ReadExcel takeData = new ReadExcel();
    TC4_Obj_FE_Registration1 FERegdObj;
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
    	driver.get(takeData.readExcelFile("testData",1,1));
    	
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
        FELoginObj = new TC0_Obj_FE_Common();
        FELoginObj.clickLoginRegisterationButton();
       
    }

 
    @Test(priority = 4)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user selects Foreign Employer and clicks on Registration button") 
    @Story("User able to find and select Foreign Employer radio button and select Registration btn")
    public void user_selects_Foreign_Employer_and_clicks_on_Registration_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_selects_Foreign_Employer_and_clicks_on_Registration_button");
        FELoginObj.selectUserTypeAsFE();
        FELoginObj.clickOnUserTypePopupRegd();
        
    }
    
    @Test(priority = 5)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Submit_btn_Text_present_And_Clicks") 
    @Story("User checks Submit Button Text and clicks it")
    public void SubmitButtonTextPresAndClicks() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Submit_btn_Text_present_And_Clicks");
    	FERegdObj = new TC4_Obj_FE_Registration1();
    	String SubmitButtonTextPre = FERegdObj.SubmitButtonTextPresent();
    	System.out.println(SubmitButtonTextPre);
    	Assert.assertEquals(SubmitButtonTextPre, "Submit");
    	FERegdObj.SubmitButtonClick();
    }

    @Test(priority = 6)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Foreign_Emp_Regd_Text_present") 
    @Story("User checks Foreign Emp Regd Text")
    public void ForeignEmpRegdTextPres() throws InterruptedException, IOException {  
    	
    	
    	test = extent.createTest("Foreign_Emp_Regd_Text_Present");
    	
    	String ForeignEmpRegdTextPre = FERegdObj.ForeignEmpRegdTextPresent();
    	System.out.println(ForeignEmpRegdTextPre);
    	Assert.assertEquals(ForeignEmpRegdTextPre, "Foreign Employer Registration");
    }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Personal_Details_Text_present") 
    @Story("User checks Personal Details Text")
    public void PersonalDetailsTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Personal_Details_Text_Present");
    	String PersonalDetailsTextPre = FERegdObj.PersonalDetailsTextPresent();
    	System.out.println(PersonalDetailsTextPre);
    	Assert.assertEquals(PersonalDetailsTextPre, "Personal Details");
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_Regd_Ident_Num_Text_present") 
    @Story("User checks Enter Regd Ident Num Text")
    public void EnterRegdIdentNumTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_Text_present");
    	String EnterRegdIdentNumTextPre = FERegdObj.EnterRegdIdentNumTextPresent();
    	System.out.println(EnterRegdIdentNumTextPre);
    	Assert.assertEquals(EnterRegdIdentNumTextPre, "Enter Registration/ Personal Identification Number*");
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_Regd_Ident_Num_Empty_validation") 
    @Story("User checks Empty Validtaion for enter regd sec")
    public void EnterRegdIdentNumEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_Empty_validation");
    	String EnterRegdIdentNumEmptyValid = FERegdObj.EnterRegdIdentNumTextValidSec();
    	System.out.println(EnterRegdIdentNumEmptyValid);
    	Assert.assertEquals(EnterRegdIdentNumEmptyValid, "This field is required");
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_regd_sec_1") //INVALID---->ab12324
    @Story("User checks  Validtaion for invalid data LESS NUMBER OF CHARACTRS")
    public void EnterRegdIdentNumInvalid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_less_char");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.EnterRegdIdentNumTextboxClick(1);
    	try {
	    	String EnterRegdIdentLessChar = FERegdObj.EnterRegdIdentNumTextValidSec();
	    	System.out.println(EnterRegdIdentLessChar);
	    	Assert.assertEquals(EnterRegdIdentLessChar, "Please enter 2 letters followed by 8 digits.");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_regd_sec_2") //INVALID---->12345643ab
    @Story("User checks  Validtaion for invalid data NUM FIRST THEN CHAR")
    public void EnterRegdIdentNumInvalid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_first");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.ClearRegdIdentNumSendText();
    	FERegdObj.EnterRegdIdentNumTextboxClick(2);
    	try {
	    	String EnterRegdIdentNumFirst = FERegdObj.EnterRegdIdentNumTextValidSec();
	    	System.out.println(EnterRegdIdentNumFirst);
	    	Assert.assertEquals(EnterRegdIdentNumFirst, "Please enter 2 letters followed by 8 digits.");
	    }
		catch(Exception e){
			Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
		}
    }
    
    @Test(priority = 12)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_regd_sec_3") //INVALID---->124532456
    @Story("User checks  Validtaion for invalid data NUM ONLY")
    public void EnterRegdIdentNumInvalid3() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_only");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.ClearRegdIdentNumSendText();
    	FERegdObj.EnterRegdIdentNumTextboxClick(3);
    	try {
	    	String EnterRegdIdentNumOnly = FERegdObj.EnterRegdIdentNumTextValidSec();
	    	System.out.println(EnterRegdIdentNumOnly);
	    	Assert.assertEquals(EnterRegdIdentNumOnly, "Please enter 2 letters followed by 8 digits.");
	    }
		catch(Exception e){
			Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
		}
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_regd_sec_4") //INVALID---->dfgf$$##
    @Story("User checks  Validtaion for invalid data SYMBOLS")
    public void EnterRegdIdentNumInvalid4() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Symbols");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.ClearRegdIdentNumSendText();
    	FERegdObj.EnterRegdIdentNumTextboxClick(4);
    	try {
    	String EnterRegdIdentSymbols = FERegdObj.EnterRegdIdentNumTextValidSec();
    	System.out.println(EnterRegdIdentSymbols);
    	Assert.assertEquals(EnterRegdIdentSymbols, "Please enter 2 letters followed by 8 digits.");
	    }
		catch(Exception e){
			Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
		}
    }
    
    @Test(priority = 14)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_regd_sec_1") //VALID---->ab12345678
    @Story("User checks  Validtaion for valid data START WITH SMALL CHAR")
    public void EnterRegdIdentNumValid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_smallchar");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.ClearRegdIdentNumSendText();
    	FERegdObj.EnterRegdIdentNumTextboxClick(5);
    	try {
    		String EnterRegdIdentSmallChar = FERegdObj.EnterRegdIdentNumTextValidSec();
        	System.out.println(EnterRegdIdentSmallChar);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID REGISTRATION NUMBER"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 15)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_regd_sec_2") //VALID---->AB12345670
    @Story("User checks  Validtaion for valid data START WITH CAPS CHAR")
    public void EnterRegdIdentNumValid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_capchar");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj.ClearRegdIdentNumSendText();
    	FERegdObj.EnterRegdIdentNumTextboxClick(6);
    	try {
    		String EnterRegdIdentCapChar = FERegdObj.EnterRegdIdentNumTextValidSec();
        	System.out.println(EnterRegdIdentCapChar);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID REGISTRATION NUMBER"+"\u001B[0m");
    	}
    }
    
    
    @Test(priority = 16)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Name_Of_Emp_Text_present") 
    @Story("User checks Name Of Emp Text")
    public void NameOfEmpTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Name_Of_Employer_Text_present");
    	String EnterNameOfEmpTextPre = FERegdObj.NameOfEmpTextPresent();
    	System.out.println(EnterNameOfEmpTextPre);
    	Assert.assertEquals(EnterNameOfEmpTextPre, "Name of Employer*");
    }
    
    @Test(priority = 17)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Name_Of_Emp_Empty_validation") 
    @Story("User checks Empty Validtaion for Name Of Emp sec")
    public void NameOfEmpEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Regd_Ident_Num_Empty_validation");
    	String EnterRegdIdentNumEmptyValid = FERegdObj.EnterNameOfEmpTextValidSec();
    	System.out.println(EnterRegdIdentNumEmptyValid);
    	Assert.assertEquals(EnterRegdIdentNumEmptyValid, "This field is required");
    }
    
    @Test(priority = 18)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Name_Of_Emp_1") //INVALID---->Sou123
    @Story("User checks Name of Emp has invalid data COMBINATION OF CHAR AND NUM")
    public void NameOfEmpInvalid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Name_of_emp_char_num_combination");
    	Thread.sleep(10000);
    	FERegdObj.EnterNameOfEmpTextboxClick(8);
    	try {
	    	String EnterEmoNameCombiNumChar = FERegdObj.EnterNameOfEmpTextValidSec();
	    	System.out.println(EnterEmoNameCombiNumChar);
	    	Assert.assertEquals(EnterEmoNameCombiNumChar, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 19)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Name_Of_Emp_2") //INVALID---->124532456
    @Story("User checks Name of Emp has invalid data ONLY NUMBERS")
    public void NameOfEmpInvalid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_Name_of_emp_only_numbers");
    	Thread.sleep(10000);
    	FERegdObj.ClearEnterNameOfEmpText();
    	FERegdObj.EnterNameOfEmpTextboxClick(9);
    	try {
	    	String EnterEmoNameCombiNumChar = FERegdObj.EnterNameOfEmpTextValidSec();
	    	System.out.println(EnterEmoNameCombiNumChar);
	    	Assert.assertEquals(EnterEmoNameCombiNumChar, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    	
    	@Test(priority = 20)
        @Severity(SeverityLevel.CRITICAL)
    	@Description("Enter_invalid_data_format_in_Name_Of_Emp_3") //INVALID---->dfgf$$##
        @Story("User checks Name of Emp has invalid data SYMBOLS")
        public void NameOfEmpInvalid3() throws InterruptedException, IOException {   	
        	
        	test = extent.createTest("Enter_Name_of_emp_symbols");
        	Thread.sleep(10000);
        	FERegdObj.ClearEnterNameOfEmpText();
        	FERegdObj.EnterNameOfEmpTextboxClick(10);
        	try {
    	    	String EnterEmoNameSymbols = FERegdObj.EnterNameOfEmpTextValidSec();
    	    	System.out.println(EnterEmoNameSymbols);
    	    	Assert.assertEquals(EnterEmoNameSymbols, "Incorrect input");
        	}
        	catch(Exception e){
        		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
        	}	
    	}
    	
    @Test(priority = 21)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_Name_Of_Emp_1") //VALID---->Sourav Padhi
    @Story("User checks Empty Validtaion has valid data PROPER DATA")
    public void NameOfEmpValid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Name_Of_Emp_Proper_1");
    	Thread.sleep(10000);
    	FERegdObj.ClearEnterNameOfEmpText();
    	FERegdObj.EnterNameOfEmpTextboxClick(11);
    	try {
    		String EnterNameOfEmpProper1 = FERegdObj.EnterNameOfEmpTextValidSec();
        	System.out.println(EnterNameOfEmpProper1);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID NAME OF EMPLOYEE"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 22)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_Name_Of_Emp_2") //VALID---->SOURAVPADHI
    @Story("User checks Empty Validtaion has valid data ALL CAPS")
    public void NameOfEmpValid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Name_Of_Emp_Proper_2");
    	Thread.sleep(10000);
    	FERegdObj.ClearEnterNameOfEmpText();
    	FERegdObj.EnterNameOfEmpTextboxClick(12);
    	try {
    		String EnterNameOfEmpProper2 = FERegdObj.EnterNameOfEmpTextValidSec();
        	System.out.println(EnterNameOfEmpProper2);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID NAME OF EMPLOYEE"+"\u001B[0m");
    	}
    }
    
    
    @Test(priority = 23)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Registration_Details_Text_present") 
    @Story("User checks Registration Details Text")
    public void RegistrationDetailsTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Registration_Details_Text_present");
    	String RegistrationDetailsTextPre = FERegdObj.RegistrationDetailsTextPresent();
    	System.out.println(RegistrationDetailsTextPre);
    	Assert.assertEquals(RegistrationDetailsTextPre, "Registration Details");
    }
    
    @Test(priority = 24)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Contact_Number_Text_present") 
    @Story("User checks Contact Number Text")
    public void ContactNumberTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Contact_Number_Text_present");
    	String ContactNumberTextPre = FERegdObj.ContactNumberPresent();
    	System.out.println(ContactNumberTextPre);
    	Assert.assertEquals(ContactNumberTextPre, "Contact number*");
    }
    
    @Test(priority = 25)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Contact_Number_Empty_validation") 
    @Story("User checks Empty Validtaion for Contact_Number sec")
    public void ContactNumberEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Contact_Number_Empty_validation");
    	String ContactNumberEmptyValid = FERegdObj.ContactNumberValidSec();
    	System.out.println(ContactNumberEmptyValid);
    	Assert.assertEquals(ContactNumberEmptyValid, "This field is required");
    }
    
    @Test(priority = 26)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Contact_number_1") //INVALID---->3453

    @Story("User checks Cntact number has invalid data LESS THAN 10 DIGITS")
    public void ContactNumInvalid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Contact_number_lessDigit");
    	Thread.sleep(10000);
    	FERegdObj.EnterContactNumberTextboxClick(14);
    	try {
	    	String EnterContactNumChar = FERegdObj.ContactNumberValidSec();
	    	System.out.println(EnterContactNumChar);
	    	Assert.assertEquals(EnterContactNumChar, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 27)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Contact_number_2") //INVALID---->4153376126323

    @Story("User checks Cntact number has invalid data MORE THAN 10 DIGITS")
    public void ContactNumInvalid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Contact_number_More_Digit");
    	Thread.sleep(10000);
    	FERegdObj.ClearContactNumberendText();
    	FERegdObj.EnterContactNumberTextboxClick(15);
    	try {
	    	String EnterContactNumChar = FERegdObj.ContactNumberValidSec();
	    	System.out.println(EnterContactNumChar);
	    	Assert.assertEquals(EnterContactNumChar, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 28)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_Contact_Num") //VALID---->8176543251
    @Story("User checks Contact Number has valid data PROPER DATA")
    public void ContactNumValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Contact_Num");
    	Thread.sleep(10000);
    	FERegdObj.ClearContactNumberendText();
    	FERegdObj.EnterContactNumberTextboxClick(16);
    	try {
    		String EnterContactNumProper = FERegdObj.ContactNumberValidSec();
        	System.out.println(EnterContactNumProper);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID CONTACT NUMBER"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 29)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Email_Text_present") 
    @Story("User checks Email Text")
    public void EmailTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_Text_present");
    	String EmailTextPre = FERegdObj.EmailPresent();
    	System.out.println(EmailTextPre);
    	Assert.assertEquals(EmailTextPre, "Email ID*");
    }
    
    @Test(priority = 30)
    @Severity(SeverityLevel.CRITICAL)
	@Description("EmailID_Empty_validation") 
    @Story("User checks Empty Validtaion for EmailID sec")
    public void EmailEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_Empty_validation");
    	String EmailEmptyValid = FERegdObj.EmailValidSec1();
    	System.out.println(EmailEmptyValid);
    	Assert.assertEquals(EmailEmptyValid, "This field is required");
    }
    
    @Test(priority = 31)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Email_1") //INVALID---->2112112
    @Story("User checks Email has invalid data INTEGER")
    public void EmailInvalid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_Integer");
    	Thread.sleep(10000);
    	FERegdObj.EnterEmailTextboxClick(18);
    	try {
	    	String EnterEmailInt = FERegdObj.EmailValidSec1();
	    	System.out.println(EnterEmailInt);
	    	Assert.assertEquals(EnterEmailInt, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 32)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Email_2") //INVALID---->sourav@try
    @Story("User checks Email has invalid data format xxxxx@xxx")
    public void EmailInvalid2() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_xxxxx@xxx");
    	Thread.sleep(10000);
    	FERegdObj.ClearEmailendText();
    	FERegdObj.EnterEmailTextboxClick(19);
    	try {
	    	String EnterEmailFormat1 = FERegdObj.EmailValidSec1();
	    	System.out.println(EnterEmailFormat1);
	    	Assert.assertEquals(EnterEmailFormat1, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 33)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Email_3") //INVALID---->vtyvyufyuu
    @Story("User checks Email has invalid data format xxxxx")
    public void EmailInvalid3() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_xxxxx");
    	Thread.sleep(10000);
    	FERegdObj.ClearEmailendText();
    	FERegdObj.EnterEmailTextboxClick(20);
    	try {
	    	String EnterEmailFormat2 = FERegdObj.EmailValidSec1();
	    	System.out.println(EnterEmailFormat2);
	    	Assert.assertEquals(EnterEmailFormat2, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 33)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Email_3") //INVALID---->test.tersting@india
    @Story("User checks Email has invalid data format xx.xx@xxxxx")
    public void EmailInvalid4() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_xx.xx@xxxxx");
    	Thread.sleep(10000);
    	FERegdObj.ClearEmailendText();
    	FERegdObj.EnterEmailTextboxClick(21);
    	try {
	    	String EnterEmailFormat3 = FERegdObj.EmailValidSec1();
	    	System.out.println(EnterEmailFormat3);
	    	Assert.assertEquals(EnterEmailFormat3, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    @Test(priority = 34)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Email_4") //INVALID---->test.tersting
    @Story("User checks Email has invalid data format xxxx.xxxx")
    public void EmailInvalid5() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_xxxx.xxxx");
    	Thread.sleep(10000);
    	FERegdObj.ClearEmailendText();
    	FERegdObj.EnterEmailTextboxClick(22);
    	try {
	    	String EnterEmailFormat4 = FERegdObj.EmailValidSec1();
	    	System.out.println(EnterEmailFormat4);
	    	Assert.assertEquals(EnterEmailFormat4, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    //-----------------------------------------------------------------------------------------------//
    @Test(priority = 35)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_Email") //VALID---->test90testung@test.com
    @Story("User checks Email has valid data PROPER DATA xxxx@xxx.xxx and gets 'Please verify Email ID' validation")
    public void EmailValid1() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Email_ID_xxxx@xxx.xxx");
    	Thread.sleep(10000);
    	FERegdObj.ClearEmailendText();
    	FERegdObj.EnterContactNumberTextboxClick(23);
    	try {
    		System.out.println("ENTERED VALID DATA BUT NUMBER IS NOT VERIFIED");
    		String EnterEmailProper1 = FERegdObj.EmailValidSec2();
	    	System.out.println(EnterEmailProper1);
	    	Assert.assertEquals(EnterEmailProper1, "Please verify Email ID");
    	}
    	catch(Exception e) {
    		
    	}
    }
    //-----------------------------------------------------------------------------------------------//
    
    @Test(priority = 36)
    @Severity(SeverityLevel.TRIVIAL)
	@Description("Get_OTP_btn_enabled_if_data_present") 
    @Story("User checks Get OTP button enabled")
    public void GetOTPEnabled() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Get_OTP_btn_enabled_if_data_present");
    	Thread.sleep(10000);
    	boolean GetOTPEnabled = FERegdObj.GetOTPButtonEnabled();
    	System.out.println(GetOTPEnabled);
    	Assert.assertEquals(GetOTPEnabled, true);
    }

    @Test(priority = 37)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Get_OTP_Text_present") 
    @Story("User checks Get OTP button Text")
    public void GetOTPTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Get_OTP_Text_present");
    	Thread.sleep(10000);
    	String GetOTPTextPre = FERegdObj.GetOTPButtonText();
    	System.out.println(GetOTPTextPre);
    	Assert.assertEquals(GetOTPTextPre, "Get OTP");
    }
    
    
    @Test(priority = 38)
    @Severity(SeverityLevel.BLOCKER)
	@Description("Registration_validity_upto_present") 
    @Story("User checks Registration validity upto Text")
    public void RegistrationvalidityuptoTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Registration_validity_upto_present");
    	String RegistrationvalidityuptoTextPre = FERegdObj.RegistrationvalidityuptoPresent();
    	System.out.println(RegistrationvalidityuptoTextPre);
    	Assert.assertEquals(RegistrationvalidityuptoTextPre, "Registration validity upto*");
    }
    
    @Test(priority = 39)
    @Severity(SeverityLevel.BLOCKER)
	@Description("Select_a_date_from_calender") 
    @Story("User able to select date in calender")
    public void UserAbleToSelectDateInCalender() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select_a_date_from_calender");
    	
    	boolean DateSelInCal = FERegdObj.selectDateFromCalender();
    	System.out.println(DateSelInCal);
    	Assert.assertEquals(DateSelInCal, true);
    }
    //-------------------------------------------------------------------------------
   
}
