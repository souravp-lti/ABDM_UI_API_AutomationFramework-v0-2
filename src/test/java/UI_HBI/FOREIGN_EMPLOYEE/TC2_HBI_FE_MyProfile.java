package UI_HBI.FOREIGN_EMPLOYEE;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_ForeignEmployee.TC0_Obj_FE_Common;
import HBI_ForeignEmployee.TC2_Obj_FE_MyProfile;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners(Allure_Listener.class) 
public class TC2_HBI_FE_MyProfile extends MainBaseFeature{
	
	TC0_Obj_FE_Common FELoginObj;
    ReadExcel takeData = new ReadExcel();
    TC2_Obj_FE_MyProfile FEmyProfile;
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
    	driver.get(takeData.readExcelFile("testData",1,1));
    	
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
        FELoginObj = new TC0_Obj_FE_Common();
        FELoginObj.clickLoginRegisterationButton();
       
    }

 
    @Test(priority = 4)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user selects Foreign Employer and clicks on Login button") 
    @Story("User able to find and select Foreign Employer radio button")
    public void user_selects_Foreign_Employer_and_clicks_on_Login_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_selects_Foreign_Employer_and_clicks_on_Login_button");
        FELoginObj.selectUserTypeAsFE();
        FELoginObj.clickOnUserTypePopupLogin();
        
    }

    @Test(priority = 5)
    @Severity(SeverityLevel.NORMAL)
	@Description("user enters Healthcare gmail com and Healthcare") 
    @Story("User enters username and password")
    public void user_enters_Healthcare_gmail_com_and_Healthcare() throws InterruptedException, IOException {
    	test = extent.createTest("user_enters_Healthcare_gmail_com_and_Healthcare");
            //FELoginObj.setEmailID(readPropertyFile("userEmail"));
    	    FELoginObj.setEmailID(takeData.readExcelFile("testData",4,1));
 
            //FELoginObj.setPassword(readPropertyFile("password"));
    	    FELoginObj.setPassword(takeData.readExcelFile("testData",5,1));
            
            FELoginObj.clickOnLoginDashboard();   
            
            try{
	            boolean withoutEmailID = FELoginObj.proceedWithoutEmailID();
	            Assert.assertEquals(withoutEmailID, false,"Cannot proceed without entering emailID");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	
	            boolean withoutPassw = FELoginObj.proceedWithoutPassword();
	            Assert.assertEquals(withoutPassw, false,"Cannot proceed without entering Password");
            }
            catch(Exception e){
            	
            }
            
            try {
	            boolean withoutEmailPassw = FELoginObj.invalidUserIDPassword();
	            Assert.assertEquals(withoutEmailPassw, false,"Invalid userID or Password");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	boolean validEmailID = FELoginObj.checkForValidUserID();
            	Assert.assertEquals(validEmailID, false,"Entered invalid UserID");
            }
            
            catch(Exception e){
            	
            }
                                
    }

    @Test(priority = 6)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_Login") 
    @Story("User able to click on Login button")
    public void click_on_Login() throws InterruptedException {
    	
    	test = extent.createTest("click_on_Login");
        System.out.println("Able to click on Login button");       
       
    }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_MyProfile") 
    @Story("User able to click on my Profile button")
    public void click_on_myProfile() throws InterruptedException, IOException {
    	
    	FEmyProfile = new TC2_Obj_FE_MyProfile();
    	test = extent.createTest("click_on_MyProfile");
    	FEmyProfile.clickOnMyProfile();       
       
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.CRITICAL)
	@Description("change_password_button") 
    @Story("User can see Change Password button")
    public void change_password_display() throws InterruptedException, IOException {
    	
    	test = extent.createTest("change_password_display");
    	boolean changePassstatus = FEmyProfile.changePasswordButton();       
        Assert.assertEquals(changePassstatus, true);
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.CRITICAL)
	@Description("edit_profile_button") 
    @Story("User can see Edit Profile button")
    public void edit_profile_display() throws InterruptedException, IOException {
    	
    	test = extent.createTest("edit_profile_display");
    	boolean editProfileStatus = FEmyProfile.editProfileButton();       
        Assert.assertEquals(editProfileStatus, true);
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Registration_Number_section_displayed") 
    @Story("User can see Registration Number section")
    public void Registration_Number_section_displayed() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registration_Number_section_displayed");
    	boolean RegistrationNumberStatus = FEmyProfile.regdNumberPresent();       
        Assert.assertEquals(RegistrationNumberStatus, true);
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.TRIVIAL)
	@Description("Registration_Number_section_value") 
    @Story("User can see Registration Number section value")
    public void Registration_Number_section_value_display() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registration_Number_section_value_display");
    	boolean RegistrationNumbersectionvalueStatus = FEmyProfile.regdNumberValuePresent();       
        Assert.assertEquals(RegistrationNumbersectionvalueStatus, true);
    }
    
    @Test(priority = 12)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Registration_Contact_Number_Present") 
    @Story("User can see Registration Contact Number")
    public void Registration_Contact_Number_Present() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registration_Contact_Number_display");
    	boolean RegistrationContactNumberStatus = FEmyProfile.regdContactPresent();       
        Assert.assertEquals(RegistrationContactNumberStatus, true);
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.BLOCKER)
	@Description("Registration _Contact_Number_section_value") 
    @Story("User can see Registration Contact Number section value")
    public void Registration_Contact_Number_section_value() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registration_Contact_Number_section_value");
    	boolean RegistrationContactNumberValStatus = FEmyProfile.regdContactNumberValuePresent();       
        Assert.assertEquals(RegistrationContactNumberValStatus, true);
    }
    
    @Test(priority = 14)
    @Severity(SeverityLevel.NORMAL)
	@Description("Registered_Email_section") 
    @Story("User can see Registered Email section")
    public void Registered_Email_section() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registered_Email_section");
    	boolean RegisteredEmailStatus = FEmyProfile.regEmailPresent();       
        Assert.assertEquals(RegisteredEmailStatus, true);
    }
    
    @Test(priority = 15)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Registered_Email_section_email") 
    @Story("User can see Registered Email section email")
    public void Registered_Email_section_email() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Registered_Email_section_email");
    	boolean RegdEmailSecemailStatus = FEmyProfile.regdEmailValuePresent();       
        Assert.assertEquals(RegdEmailSecemailStatus, true);
    }
}
