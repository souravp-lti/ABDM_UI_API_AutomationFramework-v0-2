package UI_HBI.FOREIGN_EMPLOYEE;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_ForeignEmployee.TC0_Obj_FE_Common;
import HBI_ForeignEmployee.TC2_Obj_FE_MyProfile;
import HBI_ForeignEmployee.TC3_Obj_FE_HealthcareProfessional;
import HBI_ForeignEmployee.TC4_Obj_FE_Registration1;
import HBI_ForeignEmployee.TC5_Obj_FE_Registration2;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners(Allure_Listener.class) 
public class TC5_HBI_FE_Registration2 extends MainBaseFeature{
	
	TC0_Obj_FE_Common FELoginObj;
    ReadExcel takeData = new ReadExcel();
    TC4_Obj_FE_Registration1 FERegdObj;
    TC5_Obj_FE_Registration2 FERegdObj2;
    JavascriptExecutor js = (JavascriptExecutor) driver;
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
    	driver.get(takeData.readExcelFile("testData",1,1));
    	
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
        FELoginObj = new TC0_Obj_FE_Common();
        FELoginObj.clickLoginRegisterationButton();
       
    }

 
    @Test(priority = 4)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user selects Foreign Employer and clicks on Registration button") 
    @Story("User able to find and select Foreign Employer radio button and select Registration btn")
    public void user_selects_Foreign_Employer_and_clicks_on_Registration_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_selects_Foreign_Employer_and_clicks_on_Registration_button");
        FELoginObj.selectUserTypeAsFE();
        FELoginObj.clickOnUserTypePopupRegd();
        
    }
    
    @Test(priority = 5)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Submit_btn_Text_present_And_Clicks") 
    @Story("User checks Submit Button Text and clicks it")
    public void SubmitButtonTextPresAndClicks() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Submit_btn_Text_present_And_Clicks");
    	FERegdObj = new TC4_Obj_FE_Registration1();
    	String SubmitButtonTextPre = FERegdObj.SubmitButtonTextPresent();
    	System.out.println(SubmitButtonTextPre);
    	Assert.assertEquals(SubmitButtonTextPre, "Submit");
    	FERegdObj.SubmitButtonClick1();
    	//js.executeScript("window.scrollBy(0,1000)", "");
    }

    @Test(priority = 6)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Address_main_present") 
    @Story("User checks main Address Text")
    public void AddressMainTextPres() throws InterruptedException, IOException {  
    	test = extent.createTest("Address_main_present");
    	FERegdObj2 = new TC5_Obj_FE_Registration2();
    	String AddressMainTextPre = FERegdObj2.AddressTextPresent();
    	System.out.println(AddressMainTextPre);
    	Assert.assertEquals(AddressMainTextPre, "Address");
    }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Country_Text_present") 
    @Story("User checks Country Text")
    public void CountryTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Country_Text_Present");
    	String CountryTextPre = FERegdObj2.CountryTextPresent();
    	System.out.println(CountryTextPre);
    	Assert.assertEquals(CountryTextPre, "Country*");
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Select country from dropdown") 
    @Story("User Selects value in Country dropdown")
    public void SelValueInCountryDrop() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select country from dropdown");
    	try {
    		FERegdObj2.ClickCountryDropDown();
    		Thread.sleep(10000);
        	FERegdObj2.SelValueCountryDropDown();
        	//Assert.fail("\u001B[31m"+"DROPDOWN FEATURE NOT WORKING AS EXPECTED"+"\u001B[0m");
        	System.out.println("\u001B[32m"+"COUNTRY SELECTED FROM DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
    	catch(Exception e) {
    		Assert.fail("DROPDOWN FEATURE NOT WORKING AS EXPECTED");
    	}
    	
    	
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_State_Text_present") 
    @Story("User checks State Text")
    public void StateTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("State_Text_present");
    	String StateTextPre = FERegdObj2.StateTextPresent();
    	System.out.println(StateTextPre);
    	Assert.assertEquals(StateTextPre, "State*");
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.CRITICAL)
	@Description("State_Empty_validation") 
    @Story("User checks Empty Validtaion for State")
    public void StateEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("State_Empty_validation");
    	String StateEmptyValid = FERegdObj2.StateValidSec();
    	System.out.println(StateEmptyValid);
    	Assert.assertEquals(StateEmptyValid, "This field is required");
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_State") //INVALID---->354443
    @Story("User checks  Validtaion for invalid data NUMBERS")
    public void EnterStateInvalid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("State_num");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.EnterStateTextboxClick(2);
    	try {
	    	String StateNum = FERegdObj2.StateValidSec();
	    	System.out.println(StateNum);
	    	Assert.assertEquals(StateNum, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    
    @Test(priority = 12)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_State") //VALID---->Assam
    @Story("User checks  Validtaion for valid data in State")
    public void EnterStateValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_valid_data_format_in_State");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.ClearStateText();
    	FERegdObj2.EnterStateTextboxClick(3);
    	try {
    		String StateValid = FERegdObj2.StateValidSec();
        	System.out.println(StateValid);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID STATE"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_City_Text_present") 
    @Story("User checks City Text")
    public void CityTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("City_Text_present");
    	String CityTextPre = FERegdObj2.CityTextPresent();
    	System.out.println(CityTextPre);
    	Assert.assertEquals(CityTextPre, "City/ Town/ Village*");
    }
    
    @Test(priority = 14)
    @Severity(SeverityLevel.CRITICAL)
	@Description("City_Empty_validation") 
    @Story("User checks Empty Validtaion for City")
    public void CityEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("City_Empty_validation");
    	String CityEmptyValid = FERegdObj2.CityValidSec();
    	System.out.println(CityEmptyValid);
    	Assert.assertEquals(CityEmptyValid, "This field is required");
    }
    
    @Test(priority = 15)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_City") //INVALID---->787678
    @Story("User checks  Validtaion for invalid data NUMBERS")
    public void EnterCityInvalid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("City_num");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.EnterCityTextboxClick(4);
    	try {
	    	String CityNum = FERegdObj2.CityValidSec();
	    	System.out.println(CityNum);
	    	Assert.assertEquals(CityNum, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    
    @Test(priority = 16)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_City") //VALID---->Raipur
    @Story("User checks  Validtaion for valid data in City")
    public void EnterCityValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_valid_data_format_in_City");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.ClearCityText();
    	FERegdObj2.EnterCityTextboxClick(5);
    	try {
    		String CityValid = FERegdObj2.CityValidSec();
        	System.out.println(CityValid);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID CITY"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 17)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_Address_Text_present") 
    @Story("User checks Address Text")
    public void AddressSubTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("AddressSub_Text_present");
    	String AddressSubTextPre = FERegdObj2.AddressSubTextPresent();
    	System.out.println(AddressSubTextPre);
    	Assert.assertEquals(AddressSubTextPre, "Address*");
    }
    
    @Test(priority = 18)
    @Severity(SeverityLevel.CRITICAL)
	@Description("AddressSub_Empty_validation") 
    @Story("User checks Empty Validtaion for AddressSub")
    public void AddressSubEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("AddressSub_Empty_validation");
    	String AddressSubEmptyValid = FERegdObj2.AddressSubValidSec();
    	System.out.println(AddressSubEmptyValid);
    	Assert.assertEquals(AddressSubEmptyValid, "This field is required");
    }
    
    @Test(priority = 19)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_AddressSub") //INVALID---->787678
    @Story("User checks  Validtaion for invalid data NUMBERS")
    public void EnterAddressSubInvalid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("AddressSub_num");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.EnterAddressSubClick(6);
    	try {
	    	String AddressSubNum = FERegdObj2.AddressSubValidSec();
	    	System.out.println(AddressSubNum);
	    	Assert.assertEquals(AddressSubNum, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    
    @Test(priority = 20)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_State") //VALID---->Test1234
    @Story("User checks  Validtaion for valid data in State")
    public void EnterAddressSubValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_valid_data_format_in_AddressSub");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.ClearAddressSubText();
    	FERegdObj2.EnterAddressSubClick(7);
    	try {
    		String StateValid = FERegdObj2.AddressSubValidSec();
        	System.out.println(StateValid);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID ADDRESS"+"\u001B[0m");
    	}
    }
 
    @Test(priority = 21)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_Pincode_Text_present") 
    @Story("User checks Pincode Text")
    public void PincodeTextPres() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Pincode_Text_present");
    	//FERegdObj2 = new TC5_Obj_FE_Registration2();
    	String PincodeTextPre = FERegdObj2.PincodePresent();
    	System.out.println(PincodeTextPre);
    	Assert.assertEquals(PincodeTextPre, "Pincode / Zipcode*");
    }
    
    @Test(priority = 22)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Pincode_Empty_validation") 
    @Story("User checks Empty Validtaion for Pincode")
    public void PincodeEmptValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Pincode_Empty_validation");
    	String PincodeEmptyValid = FERegdObj2.PincodeValidSec();
    	System.out.println(PincodeEmptyValid);
    	Assert.assertEquals(PincodeEmptyValid, "This field is required");
    }
    
    @Test(priority = 23)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_invalid_data_format_in_Pincode") //INVALID---->testing
    @Story("User checks  Validtaion for invalid data string")
    public void EnterPincodeInvalid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Pincode_num");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.EnterPincodeClick(8);
    	try {
	    	String PincodeNum = FERegdObj2.PincodeValidSec();
	    	System.out.println(PincodeNum);
	    	Assert.assertEquals(PincodeNum, "Incorrect input");
    	}
    	catch(Exception e){
    		Assert.fail("USER ENTERED INVALID DATA BUT NO VALIDATION DISPLAYED");
    	}
    }
    
    
    @Test(priority = 24)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter_valid_data_format_in_Pincode") //VALID---->5653146565
    @Story("User checks  Validtaion for valid data in Pincode")
    public void EnterPincodeValid() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter_valid_data_format_in_Pincode");
    	Thread.sleep(10000);
    	//FERegdObj.EnterRegdIdentNumTextboxClick();
    	FERegdObj2.ClearPincodeText();
    	FERegdObj2.EnterPincodeClick(9);
    	try {
    		String PincodeValid = FERegdObj2.PincodeValidSec();
        	System.out.println(PincodeValid);
        	Assert.fail("USER ENTERED VALID DATA BUT VALIDATION DISPLAYED");
    	}
    	catch(Exception e) {
    		System.out.println("\u001B[32m"+"ENTERED VALID PINCODE"+"\u001B[0m");
    	}
    }
    
    
    //-------------------------------------------------------------------------------
   
}
