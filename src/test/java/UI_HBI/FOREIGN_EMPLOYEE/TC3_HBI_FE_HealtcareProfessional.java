package UI_HBI.FOREIGN_EMPLOYEE;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_ForeignEmployee.TC0_Obj_FE_Common;
import HBI_ForeignEmployee.TC2_Obj_FE_MyProfile;
import HBI_ForeignEmployee.TC3_Obj_FE_HealthcareProfessional;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

@Listeners(Allure_Listener.class) 
public class TC3_HBI_FE_HealtcareProfessional extends MainBaseFeature{
	
	TC0_Obj_FE_Common FELoginObj;
    ReadExcel takeData = new ReadExcel();
    TC3_Obj_FE_HealthcareProfessional FEHealthProf;
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
    	driver.get(takeData.readExcelFile("testData",1,1));
    	
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
        FELoginObj = new TC0_Obj_FE_Common();
        FELoginObj.clickLoginRegisterationButton();
       
    }

 
    @Test(priority = 4)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user selects Foreign Employer and clicks on Login button") 
    @Story("User able to find and select Foreign Employer radio button")
    public void user_selects_Foreign_Employer_and_clicks_on_Login_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_selects_Foreign_Employer_and_clicks_on_Login_button");
        FELoginObj.selectUserTypeAsFE();
        FELoginObj.clickOnUserTypePopupLogin();
        
    }

    @Test(priority = 5)
    @Severity(SeverityLevel.NORMAL)
	@Description("user enters Healthcare gmail com and Healthcare") 
    @Story("User enters username and password")
    public void user_enters_Healthcare_gmail_com_and_Healthcare() throws InterruptedException, IOException {
    	test = extent.createTest("user_enters_Healthcare_gmail_com_and_Healthcare");
            //FELoginObj.setEmailID(readPropertyFile("userEmail"));
    	    FELoginObj.setEmailID(takeData.readExcelFile("testData",4,1));
 
            //FELoginObj.setPassword(readPropertyFile("password"));
    	    FELoginObj.setPassword(takeData.readExcelFile("testData",5,1));
            
            FELoginObj.clickOnLoginDashboard();   
            
            try{
	            boolean withoutEmailID = FELoginObj.proceedWithoutEmailID();
	            Assert.assertEquals(withoutEmailID, false,"Cannot proceed without entering emailID");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	
	            boolean withoutPassw = FELoginObj.proceedWithoutPassword();
	            Assert.assertEquals(withoutPassw, false,"Cannot proceed without entering Password");
            }
            catch(Exception e){
            	
            }
            
            try {
	            boolean withoutEmailPassw = FELoginObj.invalidUserIDPassword();
	            Assert.assertEquals(withoutEmailPassw, false,"Invalid userID or Password");
            }
            
            catch(Exception e){
            	
            }
            
            try {
            	boolean validEmailID = FELoginObj.checkForValidUserID();
            	Assert.assertEquals(validEmailID, false,"Entered invalid UserID");
            }
            
            catch(Exception e){
            	
            }
                                
    }

    @Test(priority = 6)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_Login") 
    @Story("User able to click on Login button")
    public void click_on_Login() throws InterruptedException {
    	
    	test = extent.createTest("click_on_Login");
        System.out.println("\u001B[32m"+"ABLE TO CLICK ON LOGIN BUTTON"+"\u001B[0m");       
       
        }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("click_on_HealtcareProf") 
    @Story("User able to click on HealthCare Professional button")
    public void click_on_HealthCareProfessional() throws InterruptedException, IOException {
    	
    	FEHealthProf = new TC3_Obj_FE_HealthcareProfessional();
    	test = extent.createTest("click_on_HealtcareProf");
    	FEHealthProf.clickOnHealthcareProfessional();       
       
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.NORMAL)
	@Description("SearchBox_Present") 
    @Story("User can see Searchbox")
    public void Searchbox_present() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Searchbox_present");
    	boolean SearchboxStatus = FEHealthProf.SearchBtnPresent();       
        Assert.assertEquals(SearchboxStatus, true);
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.BLOCKER)
	@Description("After_search_connect_btn") 
    @Story("User can see Searchbox")
    public void AftersearchConnectBtn() throws InterruptedException, IOException {
    	
    	test = extent.createTest("After_search_connect_btn");
    	try {
    	boolean ConnectBtnStatus = FEHealthProf.EnterValInSearchConnectBtn();       
        Assert.assertEquals(ConnectBtnStatus, true);
    	}
        
        catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.NORMAL)
	@Description("Country_DropDown_Present") 
    @Story("User can see Country DropDown")
    public void CountryDropDownPresent() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Country_DropDown_Present");
    	boolean CountryDropDownStatus = FEHealthProf.CountryDDPresent();       
        Assert.assertEquals(CountryDropDownStatus, true);
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.BLOCKER)
	@Description("Select_a_val_in_Country_DropDown") 
    @Story("User can select value from Country Drop Down")
    public void SelectValInCountryDropDown() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Select_a_val_in_Country_DropDown");
    	try {
    	FEHealthProf.enterCountryValInDD();       
        System.out.println("\u001B[32m"+"USER SELECTS VALUE IN COUNTRY DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
        
        catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}
    }
    
    @Test(priority = 12)
    @Severity(SeverityLevel.NORMAL)
	@Description("Type_DropDown_Present") 
    @Story("User can see Type DropDown")
    public void TypeDropDownPresent() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Type_DropDown_Present");
    	boolean TypeDropDownStatus = FEHealthProf.TypeDDPresent();       
        Assert.assertEquals(TypeDropDownStatus, true);
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.BLOCKER)
	@Description("Select_a_val_in_Type_DropDown") 
    @Story("User can select value from Type Drop Down")
    public void SelectValInTypeDropDown() throws InterruptedException, IOException {
    	
    	test = extent.createTest("Select_a_val_in_Type_DropDown");
    	try {
    	FEHealthProf.enterTypeValInDD();       
        System.out.println("\u001B[32m"+"USER SELECTS VALUE IN TYPE DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
        
        catch(Exception e){
    		System.out.println("\u001B[33m"+"NO RECORDS FOUND"+"\u001B[0m");
    	}
    }
    
}
