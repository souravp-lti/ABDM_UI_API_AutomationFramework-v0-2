package UI_HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.MainBaseFeature;
import HBI.HP_LandingPage;
import HBI.LoginPage;

public class HBI_HP_LandingPage_Test extends MainBaseFeature
{
	LoginPage login;
	HP_LandingPage landingPage;
	@BeforeMethod
	public void setup() throws IOException, EncryptedDocumentException, InterruptedException
	{
		initialization();
		login = new LoginPage();
		login.loginHealthcareProfessional();
		landingPage = new HP_LandingPage();	
	}
	
	@Test(groups = "Sanity")
	public void verifyInitiatedTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Initiated Tile on Dashboard Page");
		boolean value = landingPage.initiatedTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyEmployerConnectedTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Employer Connected Tile on Dashboard Page");
		boolean value = landingPage.employerConnectedTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyVerifiedTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Verified Tile on Dashboard Page");
		boolean value = landingPage.verified();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyPreDepartureOrientationTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Pre Departure Orientation Tile on Dashboard Page");
		boolean value = landingPage.preDepartureOrientationTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyVisaOrWorkPermitProcessingTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Visa Or Work PermitP rocessing Tile on Dashboard Page");
		boolean value = landingPage.visaOrWorkPermitProcessingTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyReadyForDepartureTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Ready For Departure Tile on Dashboard Page");
		boolean value = landingPage.readyForDepartureTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyEmploymentStartedTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Employment Started Tile on Dashboard Page");
		boolean value = landingPage.employmentStartedTile();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyWithdrawnRejectedTile() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Initiated Tile on Dashboard Page");
		boolean value = landingPage.withdrawnRejectedTile();
		Assert.assertEquals(value, true);
	}
	
	//Change Password on My Profile Page 
	@Test(groups = "Regression")
	public void verifyChangePassword() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Change Password on My Profile Page");
		String expectedValue = landingPage.changePassword();
		String actualValue = "Password changed successfully !!";
		Assert.assertEquals(actualValue, expectedValue);
	}
	

}
