package UI_HBI;

import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.MainBaseFeature;
import HBI.HomePage;


public class HBI_HomePage_Test extends MainBaseFeature
{
     HBI.HomePage homePage;
	
	@BeforeMethod
	public void setup() throws IOException, EncryptedDocumentException, InterruptedException
	{
		initialization();
		homePage = new HomePage();
	}
	
	@Test(groups = "Sanity")
	public void mohfwLogoTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("MOHFW Logo");
		boolean actual = homePage.mohfwLogo();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = "Sanity")
	public void azadiLogoTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Azadi Logo");
		boolean actual = homePage.azadiLogo();
		Assert.assertEquals(actual, true);	
	}
	
	@Test(groups = "Sanity")
	public void hbiLogoTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("HBI Logo");
		boolean actual = homePage.hbiLogo();
		Assert.assertEquals(actual, true);	
	}
	
	@Test(groups = "Sanity")
	public void homeButtonTest() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Home Button on Home Page");
		boolean actual = homePage.homeButton();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = {"Sanity", "Regression"})
	public void recruitersDirectoryTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Recruiters Directory Button on Home Page");
		boolean actual = homePage.recruitersDirectory();
		Assert.assertEquals(actual, true);
	}
    
	@Test(groups = {"Sanity", "Regression"})
	public void employerDirectoryTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Employers Directory Button on Home Page");
		boolean actual = homePage.employerDirectory();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = {"Sanity", "Regression"})
	public void aboutUsBtnTest() throws EncryptedDocumentException, IOException, InterruptedException
	{   
		test = extent.createTest("About Us Button");
		boolean actual = homePage.aboutUsBtn();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = "Sanity")
	public void contactUsTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Contact Us Button");
		String actual = homePage.contactUs();
		String expected = "Contact Us";
		Assert.assertEquals(actual, expected);
	}
	
	@Test(groups = {"Sanity", "Regression"})
	public void dashboardTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Dashboard Button");
		String actual = homePage.dashboardbtn();
		String expected = "Heal By India Dashboard";
		Assert.assertEquals(actual, expected);	
	}
	
	@Test(groups = {"Sanity", "Regression"})
	public void grievanceBtnTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Grievance Button");	
		String expectedurl = "https://hbigrievance.abdm.gov.in/";
		String actualUrl = homePage.grievanceBtn();
		Assert.assertEquals(actualUrl, expectedurl);
	}
	
	@Test(groups = {"Sanity", "Regression"})
	public void hpSearchBoxTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Search box on home page");		
		boolean actual = homePage.hpSearchBox();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = "Sanity")
	public void mohfwLinkAtFooterTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("MOHFW Link at Footer");
		String actualUrl = homePage.mohfwLinkAtFooter();
		String expectedUrl = "https://www.mohfw.gov.in/";
		Assert.assertEquals(actualUrl, expectedUrl);
	}
	
	@Test(groups = "Sanity")
	public void ministryOfExternalAffairsLinkAtFooterTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("MOEA Link at Footer");
		String expectedUrl = homePage.ministryOfExternalAffairsLinkAtFooter();
		String actualUrl = "https://mea.gov.in/";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void consularServiceManagementSystemLinkAtFooterTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("CSMS Link at Footer");
		String expectedUrl = homePage.consularServiceManagementSystemLinkAtFooter();
		String actualUrl = "https://portal2.madad.gov.in/AppConsular/welcomeLink";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void emigrationLinkAtFooterTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Emigration posrtal Link at Footer");
		String expectedUrl = homePage.emigrationLinkAtFooter();
		String actualUrl = "https://emigrate.gov.in/";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void cpgramsHomeLinkAtFooterTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("CPGRAMS-Home Link at Footer");
		String expectedUrl = homePage.cpgramsHomeLinkAtFooter();
		String actualUrl = "https://pgportal.gov.in/";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void youTubeLinkTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("YouTube Link at Footer");
		String expectedUrl = homePage.youTubeLink();
		String actualUrl = "https://www.youtube.com/user/mohfwindia";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void twitterLinkTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Twitter Link at Footer");
		String expectedUrl = homePage.twitterLink();
		String actualUrl = "https://twitter.com/MoHFW_INDIA";
		Assert.assertEquals(expectedUrl, actualUrl);
	}
	
	@Test(groups = "Sanity")
	public void facebooklogoTest() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Facebook Logo at Footer");
		boolean expected = homePage.facebookLogo();
		Assert.assertEquals(expected, true);
	}

}
