package UI_HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.model.Log;

import Base.MainBaseFeature;
import HBI.LoginPage;


public class HBI_LoginPage_Test extends MainBaseFeature
{
	LoginPage login;
	
	
	@BeforeMethod
	public void setup() throws IOException
	{
		initialization();
		login = new LoginPage();
	}
	
	
	@Test(groups = "Regression")
	public void loginToHBIforHPTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Login with Healthcare Professional Credentials");
		String valueExpected = login.loginToHBIforHP();
		String valueActual   = "Dr. Anshul Atul Siddhamshettiwar";
		Assert.assertEquals(valueActual, valueExpected);	
	}
	
	@Test(groups = "Regression")
	public void loginToHBIforNOTest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Login with Nodal Officer Credentials");
		String value = login.loginToHBIforNO();
		Assert.assertEquals(value, "Chandra DV");	
	}
	
	@Test(groups = "Regression")
	public void loginToHBIforRATest() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Login with Recruitement Agent Credentials");
		String value = login.loginToHbiForRA();
		Assert.assertEquals(value, "Global Healthcare career services");	
	}


}
