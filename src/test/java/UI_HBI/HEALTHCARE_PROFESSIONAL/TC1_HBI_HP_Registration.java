package UI_HBI.HEALTHCARE_PROFESSIONAL;

import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import AllureReport_Listener.Allure_Listener;
import Base.MainBaseFeature;
import HBI_HealthcareProfessional.TC0_Obj_HP_Common;
import HBI_HealthcareProfessional.TC1_Obj_HP_Registration;
import Utility.ReadExcel;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;


@Listeners(Allure_Listener.class) 
public class TC1_HBI_HP_Registration extends MainBaseFeature{
	
	TC0_Obj_HP_Common HPLoginObj;
	TC1_Obj_HP_Registration HPReg;
    ReadExcel takeData = new ReadExcel();
   
	@Test(priority = 1)
	@Severity(SeverityLevel.CRITICAL)
	@Description("user Launch Chrome browser") 
	@Story("To verify that chrome browser was launched properly")
    public void user_Launch_Chrome_browser() throws InterruptedException,IOException {
    	test = extent.createTest("user_Launch_Chrome_browser");
        initialization();   
    }
    
        
    @Test(priority = 2)
    @Severity(SeverityLevel.CRITICAL)
	@Description("user opens URL") 
    @Story("To verify that URL opened properly")
    public void user_opens_URL() throws IOException,InterruptedException {
    	test = extent.createTest("user_opens_URL");
  //  	driver.get(takeData.readExcelFile("testData",1,1));
    	driver.get(ReadExcel.readExcelFile("testData",1,1));
    }

    @Test(priority = 3)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on login Register button") 
    @Story("User able to find and click on Register button")
    public void user_clicks_on_login_Register_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
    	HPLoginObj = new TC0_Obj_HP_Common();
    	HPLoginObj.clickLoginRegisterationButton();
       
    }

    @Test(priority = 4)
    @Severity(SeverityLevel.TRIVIAL)
	@Description("user Selects HP option") 
    @Story("User able to click on healthcare professional option")
    public void user_clicks_HP_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
    	HPLoginObj.clickHPButton();
       
    }
    
    @Test(priority = 5)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on register button") 
    @Story("User able to click on register option")
    public void user_clicks_reg_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_button");
    	HPLoginObj.clickRegistrationButton();
    	HPReg = new TC1_Obj_HP_Registration();
        HPReg.SwitchToHPRWindow();
    }
    
    @Test(priority = 6)
    @Severity(SeverityLevel.BLOCKER)
	@Description("user clicks on register now button in HPR page") 
    @Story("User able to click on register now button in HPR page")
    public void user_clicks_regNow_button() throws InterruptedException, IOException {
    	test = extent.createTest("user_clicks_on_login_Register_Now_button");
        HPReg.ClickOnRegisterNowButton();
    }
    
    @Test(priority = 7)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Welcome Image in HPR page") 
    @Story("Checks Welcome Image in HPR page")
    public void verifyWelcomeImage() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Welcome_Image");
    	String WelPage = HPReg.WelcomeImgText();
    	System.out.println(WelPage);
    	Assert.assertEquals(WelPage, "Welcome!");
    }
    
    @Test(priority = 8)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks To ABDM in HPR page") 
    @Story("Checks To ABDM text in HPR page")
    public void verifyToABDM() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("ABDM_Text");
    	String ABDMtext = HPReg.ABDMText();
    	System.out.println(ABDMtext);
    	Assert.assertEquals(ABDMtext, "To Ayushman Bharat Digital Mission");
    }
    
    @Test(priority = 9)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Healthcare Professional Registry in HPR page") 
    @Story("Checks Healthcare Professional Registry text in HPR page")
    public void verifyHPR() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("HPR_Text");
    	String HPRtext = HPReg.HPRText();
    	System.out.println(HPRtext);
    	Assert.assertEquals(HPRtext, "Healthcare Professional Registry");
    }
    
    @Test(priority = 10)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Select your Registration Council in HPR page") 
    @Story("Checks Select your Registration Council text in HPR page")
    public void verifyRegdCouncil() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Regd_Council_Text");
    	String RegdCountext = HPReg.SelurRegdConText();
    	System.out.println(RegdCountext);
    	Assert.assertEquals(RegdCountext, "Select your registration council");
    }
    
    @Test(priority = 11)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Enter Registration Number text in HPR page") 
    @Story("Checks Enter Registration Number text in HPR page")
    public void verifyRegdNum() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Regd_Num_Text");
    	String RegNumtext = HPReg.EntRegdNumText();
    	System.out.println(RegNumtext);
    	Assert.assertEquals(RegNumtext, "Enter registration number");
    }
    
    @Test(priority = 12)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Category in HPR page") 
    @Story("Checks Category text in HPR page")
    public void verifyToCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Category_Text");
    	String Cattext = HPReg.SelCateText();
    	System.out.println(Cattext);
    	Assert.assertEquals(Cattext, "Select Category");
    }
    
    @Test(priority = 13)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Checks Sub Category text in HPR page") 
    @Story("Checks Sub Category text in HPR page")
    public void verifyToSubCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Sub Cate_Text");
    	String SubCatetext = HPReg.SelSubCateText();
    	System.out.println(SubCatetext);
    	Assert.assertEquals(SubCatetext, "Select Sub Category");
    }
    
    @Test(priority = 14)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Submit Button is present") 
    @Story("Checks Submit Button is present")
    public void SubmitBtnPresent() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Submit_Button_present");
    	Thread.sleep(7000);
    	boolean SubmitBtnPre = HPReg.SubmitBtnVisible();
    	System.out.println(SubmitBtnPre);
    	Assert.assertEquals(SubmitBtnPre, true);
    }
    
    @Test(priority = 15)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Submit Button text") 
    @Story("Checks Submit Button text is present")
    public void SubmitBtnTxtPresent() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Submit_Button_present");
    	String SubmitBtnPreText = HPReg.SubmitText();
    	System.out.println(SubmitBtnPreText);
    	Assert.assertEquals(SubmitBtnPreText, "Submit");
    }
    
    @Test(priority = 16)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Reset Button is present") 
    @Story("Checks Reset Button is present")
    public void ResetBtnPresent() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Reset_Button_present");
    	boolean ResetBtnPre = HPReg.ResetBtnVisible();
    	System.out.println(ResetBtnPre);
    	Assert.assertEquals(ResetBtnPre, true);
    }
    
    @Test(priority = 17)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Reset Button text") 
    @Story("Checks Reset Button text is present")
    public void ResetBtnTxtPresent() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Reset_Button_present");
    	String ResetBtnPreText = HPReg.ResetText();
    	System.out.println(ResetBtnPreText);
    	Assert.assertEquals(ResetBtnPreText, "Reset");
    }
    
    @Test(priority = 18)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Select your registration council not selected validation") 
    @Story("Checks Validation message when registration council is not selected and clicked on Submit button")
    public void RegdCounEmptyVal() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Regd_Council_Empty_Val");
    	HPReg.SubmitBtnClick();
    	String CouncilEmpty = HPReg.CouncilVal();
    	System.out.println(CouncilEmpty);
    	Assert.assertEquals(CouncilEmpty, "Please select registration council");
    }
    
    @Test(priority = 19)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Enter registration number not selected validation") 
    @Story("Checks Validation message when registration number is not selected and clicked on Submit button")
    public void RegdNumEmptyVal() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Regd_Number_Empty_Val");
    	String NumEmpty = HPReg.RegdNumVal();
    	System.out.println(NumEmpty);
    	Assert.assertEquals(NumEmpty, "Please enter registration number");
    }
    
    @Test(priority = 20)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Category not selected validation") 
    @Story("Checks Validation message when Category is not selected and clicked on Submit button")
    public void CateEmptyVal() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Cate_Empty_Val");
    	String CateEmpty = HPReg.CateVal();
    	System.out.println(CateEmpty);
    	Assert.assertEquals(CateEmpty, "Please select category");
    }
    
    @Test(priority = 21)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Sub Category not selected validation") 
    @Story("Checks Validation message when Sub Category is not selected and clicked on Submit button")
    public void SubCateEmptyVal() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Sub_Cate_Empty_Val");
    	String SubCateEmpty = HPReg.SubCateVal();
    	System.out.println(SubCateEmpty);
    	Assert.assertEquals(SubCateEmpty, "Please select sub category");
    }
    
    @Test(priority = 22)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Select Regd Council from dropdown") 
    @Story("User Selects value in Regd Council")
    public void SelValueInRegdCounc() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Regd Council from dropdown");
    	try {
    		HPReg.ClickRegdCouncildropDown();
    		Thread.sleep(5000);
    		HPReg.SearchValdropDown(1);
    		HPReg.SelValueListdropDown(1);
    		Thread.sleep(5000);
        	System.out.println("\u001B[32m"+"REGISTRATION COUNCIL SELECTED FROM DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
    	catch(Exception e) {
    		Assert.fail("REGISTRATION COUNCIL DROPDOWN FEATURE NOT WORKING AS EXPECTED");
    	}
    }
    
    @Test(priority = 23)
    @Severity(SeverityLevel.CRITICAL)
	@Description("After selecting Regd Council from dropdown the validation should be removed") 
    @Story("User Selects value in Regd Council and validation is removed")
    public void ValiRemoveValueInRegdCounc() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Regd Council from dropdown and validation should be removed");
    	try {
    		HPReg.SubmitBtnClick();
    		Thread.sleep(5000);
    		String val = HPReg.CouncilVal();
    		if(val == "Please select registration council") {
    			Assert.fail("ENTERED VALID VALUE FROM THE REGD. COUNCIL DROPDOWN AND CLICKED ON ENTER BUT STILL SEEING THE VALIDATION");
    		}
    		else {
    			System.out.println("\u001B[32m"+"ENTERED VALID VALUE FROM THE REGD. COUNCIL DROPDOWN AND CLICKED ON ENTER --VALIDATION REMOVED"+"\u001B[0m");
    		}
    	}
    	catch(Exception e) {
    		
    	}
    }
    
    @Test(priority = 24)
    @Severity(SeverityLevel.CRITICAL)
	@Description("After entering value in registration number and clicking on submit validation should be removed") 
    @Story("User Selects value in registration number and validation is removed")
    public void ValiRemoveValueInRegdNum() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Enter value in Registration number and validation should be removed");
    	try {
    		//HPReg.SubmitBtnClick();
    		Thread.sleep(5000);
    		HPReg.EnterRegdNum();
    		Thread.sleep(5000);
    		HPReg.SubmitBtnClick();
    		Thread.sleep(5000);
    		String val = HPReg.RegdNumVal();
    		System.out.println(val);
    		if(val == "Please enter registration number") {
    			Assert.fail("ENTERED VALID VALUE IN REGISTRATION NUMBER BOX AND CLICKED ON ENTER BUT STILL SEEING THE VALIDATION");
    		}
    		else {
    			System.out.println("\u001B[32m"+"ENTERED VALID VALUE FROM THE REGD. NUMBER BOX AND CLICKED ON ENTER --VALIDATION REMOVED"+"\u001B[0m");
    		}
    	}
    	catch(Exception e) {
    		
    	}
    }
    
    @Test(priority = 25)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Select Category from dropdown") 
    @Story("User Selects value in Category")
    public void SelValueInCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Category from dropdown");
    	try {
    		HPReg.CategoryropDown();
    		Thread.sleep(5000);
    		HPReg.SearchValdropDown(3);
    		HPReg.SelValueListdropDown(3);
    		Thread.sleep(5000);
        	System.out.println("\u001B[32m"+"CATEGORY SELECTED FROM DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
    	catch(Exception e) {
    		Assert.fail("CATEGORY DROPDOWN FEATURE NOT WORKING AS EXPECTED");
    	}
    }
    
    @Test(priority = 26)
    @Severity(SeverityLevel.CRITICAL)
	@Description("After selecting Category from dropdown the validation should be removed") 
    @Story("User Selects value in Category and validation is removed")
    public void ValiRemoveValueInCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Category from dropdown and validation should be removed");
    	try {
    		HPReg.SubmitBtnClick();
    		Thread.sleep(5000);
    		String val = HPReg.CateVal();
    		if(val == "Please select category") {
    			Assert.fail("ENTERED VALID VALUE FROM THE CATEGORY DROPDOWN AND CLICKED ON ENTER BUT STILL SEEING THE VALIDATION");
    		}
    		else {
    			System.out.println("\u001B[32m"+"ENTERED VALID VALUE FROM THE CATEGORY DROPDOWN AND CLICKED ON ENTER --VALIDATION REMOVED"+"\u001B[0m");
    		}
    	}
    	catch(Exception e) {
    		
    	}
    }
    
    @Test(priority = 27)
    @Severity(SeverityLevel.CRITICAL)
	@Description("Select Sub Category from dropdown") 
    @Story("User Selects value in Sub Category")
    public void SelValueInSubCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Sub Category from dropdown");
    	try {
    		HPReg.SubCatedropDown();
    		Thread.sleep(5000);
    		HPReg.SearchValdropDown(4);
    		HPReg.SelValueListdropDown(4);
    		Thread.sleep(5000);
        	System.out.println("\u001B[32m"+"SUB CATEGORY SELECTED FROM DROPDOWN SUCCESSFULLY"+"\u001B[0m");
    	}
    	catch(Exception e) {
    		Assert.fail("SUB CATEGORY DROPDOWN FEATURE NOT WORKING AS EXPECTED");
    	}
    }
    
    @Test(priority = 28)
    @Severity(SeverityLevel.CRITICAL)
	@Description("After selecting Sub Category from dropdown the validation should be removed") 
    @Story("User Selects value in Sub Category and validation is removed")
    public void ValiRemoveValueInSubCate() throws InterruptedException, IOException {   	
    	
    	test = extent.createTest("Select Sub Category from dropdown and validation should be removed");
    	try {
    		HPReg.SubmitBtnClick();
    		Thread.sleep(5000);
    		String val = HPReg.SubCateVal();
    		if(val == "Please select sub category") {
    			Assert.fail("ENTERED VALID VALUE FROM THE SUB CATEGORY DROPDOWN AND CLICKED ON ENTER BUT STILL SEEING THE VALIDATION");
    		}
    		else {
    			System.out.println("\u001B[32m"+"ENTERED VALID VALUE FROM THE SUB CATEGORY DROPDOWN AND CLICKED ON ENTER --VALIDATION REMOVED"+"\u001B[0m");
    		}
    	}
    	catch(Exception e) {
    		
    	}
    }
 
}
