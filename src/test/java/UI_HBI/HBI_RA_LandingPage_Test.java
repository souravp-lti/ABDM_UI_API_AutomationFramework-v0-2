package UI_HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.MainBaseFeature;
import HBI.LoginPage;
import HBI.RA_LandingPage;

public class HBI_RA_LandingPage_Test extends MainBaseFeature
{
	RA_LandingPage landingPage;
	LoginPage login;
	
	@BeforeMethod
	public void setup() throws IOException, InterruptedException
	{
		initialization();
		login  = new LoginPage();
		login.loginRecruitementAgent();
	    landingPage = new RA_LandingPage();
	}
	
	@Test(groups = "Sanity")
	public void verifyInitiatedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Initiated Tile");
		String expectedStatus = "Initiated";
		String actualStatus = landingPage.initiatedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyEmployerConnectedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Employer Connected Tile");
		String expectedStatus = "Employer connected";
		String actualStatus = landingPage.employerConnectedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyVerifiedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Verified Connected Tile");
		String expectedStatus = "Verified";
		String actualStatus = landingPage.verifiedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyPreDepartureOrientationTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Pre Departure Orientation Tile");
//		String expectedStatus = "Pre Departure orientation";
		String actualStatus = landingPage.preDepartureOrientationTile();
		Assert.assertEquals(actualStatus, "No Result Found", "Pre Departure orientation");
	}
	
	@Test(groups = "Sanity")
	public void verifyVisaWorkPermitProcessingTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Visa or Work Permit Processing Tile");
		String expectedStatus = "Visa or Work Permit processing";
		String actualStatus = landingPage.visaWorkPermitProcessingTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyReadyForDepartureTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Ready For Departure Tile");
		String expectedStatus = "Ready for departure";
		String actualStatus = landingPage.readyForDepartureTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyEmployementStartedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Employement Started Tile");
		String expectedStatus = "Employement started";
		String actualStatus = landingPage.employementStartedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyWithdrawnRejectedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Withdrawn Rejected Tile");
		String expectedStatus = "Withdrawn/Rejected";
		String actualStatus = landingPage.employerConnectedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Sanity")
	public void verifyHealthcareProfessionalSearchBox() throws EncryptedDocumentException, InterruptedException, IOException
	{
		test = extent.createTest("Healthcare Professional Search");
		boolean actual = landingPage.healthcareProfessionalSearchBox();
		Assert.assertEquals(actual, true);
	}
	
	@Test(groups = "Regression")
	public void verifyChangePassword() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Change Password");
		String actualText = landingPage.changePassword();
		String expectedtext = "Password changed successfully !!";
		Assert.assertEquals(expectedtext, actualText);
	}
	
	@Test(groups = "Sanity")
	public void verifyEnterRegistrationNumberTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Enter Registration Number Textbox on Edit Profile page");
		boolean value = landingPage.enterRegistrationNumberTextbox();
		Assert.assertEquals(value, true);
	}
	
	@Test(groups = "Regression")
	public void verifyNameOfRecruitingAgencyTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Name of Recruiting Agency Textbox on Edit Profile page");
		String expectedValue = "Global Healthcare career services ";
		String actualValue = landingPage.nameOfRecruitingAgencyTextbox();
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	@Test(groups = "Sanity")
	public void verifyContactNumberTextbox() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Contact Number Textbox on Edit Profile page");
		boolean actualValue = landingPage.contactNumberTextbox();
		Assert.assertEquals(actualValue, true);
	}
	
	@Test(groups = "Sanity")
	public void verifyEmailIDTextbox() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Email ID Textbox on Edit Profile page");
		boolean actualValue = landingPage.emailIDTextbox();
		Assert.assertEquals(actualValue, true); 
	}
	
	@Test(groups = "Sanity")
	public void verifyRegistrationvalidUpto() throws EncryptedDocumentException, IOException
	{
		test = extent.createTest("Registration valid upto calender on Edit Profile page");
		boolean actualValue = landingPage.registrationvalidUpto();
		Assert.assertEquals(actualValue, true);
	}
	
	@Test(groups = "Sanity")
	public void verifySelectTheCountryRecruitingForDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Select the country recruiting for dropdown on Edit Profile page");
        boolean actualValue = landingPage.selectTheCountryRecruitingForDropdown();
        Assert.assertEquals(actualValue, true);  
	}
	
	@Test(groups = "Sanity")
	public void verifySelectTheRecruitingHealthcareProfessionalTypeDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Select The Recruiting Healthcare Professional Type Dropdown on Edit Profile page");
        boolean actualValue = landingPage.selectTheRecruitingHealthcareProfessionalTypeDropdown();
        Assert.assertEquals(actualValue, true);  
	}
	
	@Test(groups = "Sanity")
	public void verifyCountryTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Country textbox on Edit Profile page");
        boolean actualValue = landingPage.countryTextbox();
        Assert.assertEquals(actualValue, false);  
	}
	
	@Test(groups = "Sanity")
	public void verifyStateDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("State dropdown on Edit Profile page");
        boolean actualValue = landingPage.stateDropdown();
        Assert.assertEquals(actualValue, true);  
	}
	
	@Test(groups = "Sanity")
	public void verifyCityDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("City dropdown on Edit Profile page");
        boolean actualValue = landingPage.cityDropdown();
        Assert.assertEquals(actualValue, true);  
	}
	
	@Test(groups = "Sanity")
	public void verifyAddressDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Address textbox on Edit Profile page");
        boolean actualValue = landingPage.adressTextbox();
        Assert.assertEquals(actualValue, true);  
	}
	
	@Test(groups = "Sanity")
	public void verifyPincodeDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Pincode textbox on Edit Profile page");
        boolean actualValue = landingPage.pincodeTextbox();
        Assert.assertEquals(actualValue, true);  
	}
	
	
	@Test(groups = "Regression")
	public void verifyEditProfile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Edit Profile Functionality");
        String actualValue = landingPage.editProfile();
        String expectedValue = "Information has been updated and submitted successfully.";
        Assert.assertEquals(actualValue, expectedValue);  
	}
	
	
	
	
	
	
	

}
