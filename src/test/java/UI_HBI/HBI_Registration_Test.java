package UI_HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.MainBaseFeature;
import HBI.HP_LandingPage;
import HBI.LoginPage;
import HBI.Registration;
import Utility.JDBC_Connection;

public class HBI_Registration_Test extends MainBaseFeature
{
    Registration reg;
 
	
	@BeforeMethod
	public void setup() throws IOException, EncryptedDocumentException, InterruptedException
	{
		initialization();	
		reg = new Registration();
	}
	
/* ---------------------Nodal Officer---------------------
	 * ---------------------------------------------------------------------	
*/	
	@Test(groups = "Sanity")
	public void verifyLabel() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Registration of Nodal Officer Label");
		String expextedValue = "Nodal Officer Registration";
		String actualValue = reg.nodalOfficerLabel();
		Assert.assertEquals(expextedValue, actualValue);
	}
	
	@Test(groups = "Sanity")
	public void verifyNameOfAuthorisedOfficialTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Name Of Authorised Official Textbox");
		String expectedValue = "Anshul Siddham";
		String actualValue = reg.nameOfAuthorisedOfficialTextbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
	@Test(groups= "Sanity")
	public void verifyAuthorisedOfficialTextbox() throws EncryptedDocumentException, InterruptedException, IOException
	{
		test = extent.createTest("Authorised Official Textbox");
		String expectedValue = "Healthcare";
		String actualValue = reg.authorisedOfficialTextbox();
		Assert.assertEquals(expectedValue, actualValue);

	}

//Designation textbox	
	@Test(groups= "Sanity")
	public void verifyDesignationTextbox() throws EncryptedDocumentException, InterruptedException, IOException
	{
		test = extent.createTest("Designation Textbox");
		String expectedValue = "Nodal Officer";
		String actualValue = reg.designation();
		Assert.assertEquals(expectedValue, actualValue);

	}

//Create Password	
	@Test(groups= "Sanity")
	public void verifyCreatePasswordTextbox() throws EncryptedDocumentException, InterruptedException, IOException
	{
		test = extent.createTest("Create Password Textbox");
		String expectedValue = "Dv@123456";
		String actualValue = reg.createPasswordTextbox();
		Assert.assertEquals(expectedValue, actualValue);

	}
	
//Confirm Password	
		@Test(groups= "Sanity")
		public void verifyConfirmPasswordTextbox() throws EncryptedDocumentException, InterruptedException, IOException
		{
			test = extent.createTest("Confirm Password Textbox");
			String expectedValue = "Dv@123456";
			String actualValue = reg.confirmPasswordTextbox();
			Assert.assertEquals(expectedValue, actualValue);

		}

//Contact Number textbox		
		@Test(groups= "Sanity")
		public void verifycontactNumberTextbox() throws EncryptedDocumentException, InterruptedException, IOException
		{
			test = extent.createTest("Contact Number Textbox and get otp button");
			String expectedValue = "Contact Number OTP Verification";
			String actualValue = reg.contactNumberTextbox();
			Assert.assertEquals(expectedValue, actualValue);

		}
		
//Email ID Number textbox		
		@Test(groups= "Sanity")
		public void verifyEmailIDTextbox() throws EncryptedDocumentException, InterruptedException, IOException
		{
			test = extent.createTest("Email ID Textbox and get otp button");
			String expectedValue = "Email OTP Verification";
			String actualValue = reg.emailIDTextbox();
			Assert.assertEquals(expectedValue, actualValue);

		}
	
		
	
//Complete Registration flow for Nodal Officer 	
	@Test(groups = "Regression", enabled = false)
	public void verifyRegistrationOfNO() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Registration of Nodal Officer");
		String actualValue = reg.registrationOfNO();
		String expectedValue = "Congratulations";
		Assert.assertEquals(actualValue, expectedValue);
	}
	
/* ---------------------Recrutement Agent---------------------
 * ---------------------------------------------------------------------	
 */	

	
	@Test(groups = "Sanity")
	public void verifyRALabel() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Registration of Recruiting Agent Label");
		String expextedValue = "Recruiting Agent Registration";
		String actualValue = reg.recruitementAgentLabel();
		Assert.assertEquals(expextedValue, actualValue);
	}
	
//Enter Registration Number Textbox
	@Test(groups = "Sanity")
	public void verifyEnterRegistrationNumberTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Enter Registration Number Textbox");
		String expextedValue = "An123456";
		String actualValue = reg.enterRegistrationNumberTextbox();
		Assert.assertEquals(expextedValue, actualValue);
	}
	
//Name Of Recruiting Agency Textbox	
	@Test(groups = "Sanity")
	public void verifyNameOfRecruitingAgencyTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Name Of Recruiting Agency Textbox");
		String expextedValue = "Sunpharma Care";
		String actualValue = reg.nameOfRecruitingAgencyTextbox();
		Assert.assertEquals(expextedValue, actualValue);
	}
	
//Contact Number Textbox And Get Otp Btn 
	@Test(groups = "Sanity")
	public void verifyContactNumberTextboxAndGetOtpBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Contact Number Textbox And Get Otp Btn");
		String expextedValue = "Contact Number OTP Verification";
		String actualValue = reg.contactNumberTextboxAndGetOtpBtn();
		Assert.assertEquals(expextedValue, actualValue);
	}
	
//Email ID Textbox And Get Otp Btn
	@Test(groups = "Sanity")
	public void verifyEmailIDTextboxAndGetOtpBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Email ID Textbox And Get Otp Btn");
		String expextedValue = "Email OTP Verification";
		String actualValue = reg.emailIDTextboxAndGetOtpBtn();
		Assert.assertEquals(expextedValue, actualValue);
	}
	

//Registration Valid upto calender	
	@Test(groups = "Sanity")
	public void verifyRegistrationValidUptoDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Registration Valid Upto Calender");
		String expextedValue = "30/10/2022";
		String actualValue = reg.registrationValidUptoDropdown();
		Assert.assertEquals(expextedValue, actualValue);
	}

//Select the country recruiting for listbox
	@Test(groups = "Sanity")
	public void verifySelectTheCountryRecruitingForTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Select The Country Recruiting For Textbox");
		String expectedValue = "India";
		String actualValue = reg.selectTheCountryRecruitingForListbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//Select the Recruitement Healthcare Professional Type Listbox
	@Test(groups = "Sanity")
	public void verifySelectRecruitingHealthcareProfessionalTypeListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Select Recruiting Healthcare Professional Type Listbox");
		String expectedValue = "Optometrist";
		String actualValue = reg.selectRecruitingHealthcareProfessionalTypeListbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//Country Textbox
	@Test(groups = "Sanity")
	public void verifyCountryTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Country Textbox");
		String expectedValue = "INDIA";
		String actualValue = reg.countryTextbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//State Selection Listbox
	@Test(groups = "Sanity")
	public void verifyStateSelectionListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("State Textbox");
		String expectedValue = "MAHARASHTRA";
		String actualValue = reg.stateListybox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//City/District selection listbox
	@Test(groups = "Sanity")
	public void verifyCityListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("City/District Textbox");
		String expectedValue = "NAGPUR";
		String actualValue = reg.cityListbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//Address textbox
	@Test(groups = "Sanity")
	public void verifyaddressTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Address Textbox");
		boolean expectedValue = true;
		boolean actualValue = reg.addresstextbox();
		Assert.assertEquals(expectedValue, actualValue);
	}
	
//Pincode Textbox
	@Test(groups = "Sanity")
	public void verifyPincodeTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Pincode Textbox");
		String expectedValue = "123456";
		String actualValue = reg.pincodeTextbox();
		Assert.assertEquals(expectedValue, actualValue);
		
	}
	
//Complete Registration flow for Recruitement Agent	
	@Test(groups = "Regression", enabled = false)
	public void verifyRegistrationOfRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Registration of Recruitement Agent");
		String actualValue = reg.registrationOfRA();
		String expectedValue = "Congratulations";
		Assert.assertEquals(actualValue, expectedValue);
	}
	
}
