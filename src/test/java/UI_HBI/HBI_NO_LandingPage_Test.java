package UI_HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Base.MainBaseFeature;
import HBI.LoginPage;
import HBI.NO_LandingPage;
import HBI.RA_LandingPage;

public class HBI_NO_LandingPage_Test extends MainBaseFeature
{
	NO_LandingPage landingPage;
	LoginPage login;
	
	@BeforeMethod
	public void setup() throws IOException, InterruptedException
	{
		initialization();
		login  = new LoginPage();
		login.loginNodalOfficer();
	    landingPage = new NO_LandingPage();
	}
	
	@Test(groups = "Sanity")
	public void verifyInitiatedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Initiated Tile");
		String expectedStatus = "Initiated";
		String actualStatus = landingPage.initiatedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyEmployerConnectedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Employer Connected Tile");
		String expectedStatus = "Employer connected";
		String actualStatus = landingPage.employerConnectedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyVerifiedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Verified Connected Tile");
		String expectedStatus = "Verified";
		String actualStatus = landingPage.verifiedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyPreDepartureOrientationTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Pre Departure Orientation Tile");
		String expectedStatus = "Pre-departure orientation";
		String actualStatus = landingPage.preDepartureOrientationTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyVisaWorkPermitProcessingTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Visa or Work Permit Processing Tile");
		String expectedStatus = "Visa or Work Permit processing";
		String actualStatus = landingPage.visaWorkPermitProcessingTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyReadyForDepartureTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Ready For Departure Tile");
		String expectedStatus = "Ready for departure";
		String actualStatus = landingPage.readyForDepartureTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = {"Sanity","Regression"})
	public void verifyEmployementStartedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Employement Started Tile");
		String expectedStatus = "Employment started";
		String actualStatus = landingPage.employementStartedTile();
		Assert.assertEquals(actualStatus, expectedStatus);
	}
	
	@Test(groups = "Regression")
	public void verifySearchFunctionalityOnDashboardForRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Search Functionality On Dashboard For RA");
		String expectedType = "Recruiting Agent";
		String actualType = landingPage.searchFunctionalityOnDashboardForRA();
		Assert.assertEquals(actualType, expectedType);
	}
	
	@Test(groups = "Regression")
	public void verifySearchFunctionalityOnDashboardForFE() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Search Functionality On Dashboard For FE");
		String expectedType = "Foreign Employer";
		String actualType = landingPage.searchFunctionalityOnDashboardForFE();
		Assert.assertEquals(actualType, expectedType);
	}
	
	@Test(groups = "Regression")
	public void verifyUserTypeFilterFunctionalityOnApprovalPendingPageForRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("User Type Filter Functionality On Approval Pending Page For RA");
		String expectedType = "Recruiting Agent";
		String actualType = landingPage.userTypeFilterFunctionalityOnApprovalPendingPageForRA();
		Assert.assertEquals(actualType, expectedType);
	}
	
	@Test(groups = "Regression")
	public void verifyUserTypeFilterFunctionalityOnApprovalPendingPageForFE() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("User Type Filter Functionality On Approval Pending Page For FE");
		String expectedType = "Foreign Employer";
		String actualType = landingPage.userTypeFilterFunctionalityOnApprovalPendingPageForFE();
		Assert.assertEquals(actualType, expectedType);
	}
	
	@Test(groups = "Regression")
	public void verifyUserTypeFilterFunctionalityOnApprovalPendingPageForNO() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("User Type Filter Functionality On Approval Pending Page For NO");
		String expectedType = "Nodal Officer";
		String actualType = landingPage.userTypeFilterFunctionalityOnApprovalPendingPageForNO();
		Assert.assertEquals(actualType, expectedType);
	}
	
	@Test(groups = "Sanity")
	public void verifySubmittedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Submitted Tile On Approval Pending Page");
		Assert.assertEquals(true, landingPage.submittedTileOnApprovalPendingPage());
	}
	
	@Test(groups = "Sanity")
	public void verifyApprovedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Submitted Tile On Approval Pending Page");
		Assert.assertEquals(true, landingPage.approvedTileOnApprovalPendingPage());
	}
	
	@Test(groups = "Sanity")
	public void verifyRejectedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Rejected Tile On Approval Pending Page");
		Assert.assertEquals(true, landingPage.rejectedTileOnApprovalPendingPage());
	}
	
	@Test(groups = "Sanity")
	public void verifySuspendedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Suspended Tile On Approval Pending Page");
		Assert.assertEquals(true, landingPage.suspendedTileOnApprovalPendingPage());
	}
	
	@Test(groups = "Sanity")
	public void verifyDeactivatedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Deactivated Tile On Approval Pending Page");
		Assert.assertEquals(true, landingPage.deactivatedTileOnApprovalPendingPage());
	}
	
	// Edit Profile	section 
	// name of Authorised Official textbox
	@Test(groups = "Sanity")
	public void verifyNameOfAuthorisedOfficialTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Name Of Authorised Official Textbox");
		Assert.assertEquals(true, landingPage.nameOfAuthorisedOfficialTextbox());
	}
	
	@Test(groups = "Sanity")
	public void verifyAuthorisedOfficialTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Authorised Official Textbox");
		Assert.assertEquals(true, landingPage.authorisedOfficialTextbox());
	}
	
	@Test(groups = "Sanity")
	public void verifyDesignationTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Designation Textbox");
		Assert.assertEquals(true, landingPage.designationTextbox());
	}
	
	@Test(groups = "Sanity")
	public void verifyContactNumberTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Contact Numnber Textbox");
		Assert.assertEquals(true, landingPage.contactNumberTextbox());
	}
	
	@Test(groups = "Sanity")
	public void verifyEmailIDTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Email ID Number Textbox");
		Assert.assertEquals(true, landingPage.emailIDTextbox());
	}
	
	@Test(groups = "Sanity")
	public void verifygetOTP() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Get Otp button");
		Assert.assertEquals(true, landingPage.getOtpButton());
	}
	
	@Test(groups = "Sanity")
	public void verifyUpdateAndSubmitBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Update And Submit button");
		Assert.assertEquals(true, landingPage.updateAndSubmitButton());
	}
	
//Change Password Functionality	
	@Test(groups = "Regression")
	public void verifyChangePassword() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Change Password");
		String actualText = landingPage.changePassword();
		String expectedtext = "Password changed successfully !!";
		Assert.assertEquals(expectedtext, actualText);
	}
	
//Edit Profile
	@Test(groups = "Regression")
	public void verifyEditProfile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		test = extent.createTest("Edit Profile");
		String actualText = landingPage.editProfile();
		String expectedtext = "Information has been updated and submitted successfully.";
		Assert.assertEquals(expectedtext, actualText);
	}
}
