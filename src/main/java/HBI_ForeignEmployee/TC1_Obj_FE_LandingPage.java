package HBI_ForeignEmployee;

import java.io.IOException;



import org.openqa.selenium.By;
import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC1_Obj_FE_LandingPage extends MainBaseFeature{
    



   ReadExcel takeData = new ReadExcel();
    
    
    @Step("OBJ-1")
    public boolean dashBoardVisible() throws InterruptedException, IOException {   
        //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        Thread.sleep(10000);
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_LandingPage",1, 1))).isDisplayed();
    }
//----------------------------------------------------------------------------------------------------------//
    @Step("OBJ-2")
    public String TabDisplayed(int i) throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_LandingPage",i, 1))).getText();
    }
    
    @Step("OBJ-3")
    public String Numberdashboard(int i) throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_LandingPage",i, 1))).getText();
    }
    
    @Step("OBJ-4")
    public String statusInRecDisplayed() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Common",13, 1))).getText();
    }
    
    @Step("OBJ-4")
    public String noResultFound() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Common",14, 1))).getText();
    }
    
    @Step("OBJ-5")
    public void clickontab(int i) throws InterruptedException, IOException {    
        driver.findElement(By.xpath(takeData.readExcelFile("FE_LandingPage",i, 1))).click();
        //driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
        Thread.sleep(10000);
    }
    
//----------------------------------------------------------------------------------------------------------//    
    @Step("OBJ-6")
    public String ListNumber() throws InterruptedException, IOException {    
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_LandingPage",4, 1))).getText();
    }
}
