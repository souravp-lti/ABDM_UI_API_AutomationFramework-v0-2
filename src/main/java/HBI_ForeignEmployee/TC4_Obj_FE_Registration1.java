package HBI_ForeignEmployee;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;

public class TC4_Obj_FE_Registration1 extends MainBaseFeature
{

	ReadExcel takeData = new ReadExcel();
    JavascriptExecutor js = (JavascriptExecutor) driver;

@Step("Submit Button text")
public String SubmitButtonTextPresent() throws InterruptedException, IOException {
    WebElement submitBtn = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",22, 1)));
    js.executeScript("window.scrollBy(0,300)", "");
    Thread.sleep(10000);
    return submitBtn.getText();
}

@Step("Submit Button click")
public void SubmitButtonClick() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",22, 1))).click();
    js.executeScript("window.scrollBy(0,-300)", "");
    Thread.sleep(10000);
    }

@Step("Submit Button click")
public void SubmitButtonClick1() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",22, 1))).click();
    //js.executeScript("window.scrollBy(0,-300)", "");
    Thread.sleep(10000);
    }

@Step("Foreign Employer Registration text present")
public String ForeignEmpRegdTextPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",1, 1))).getText();
}

@Step("Personal Details text present")
public String PersonalDetailsTextPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",2, 1))).getText();
}

@Step("Enter Regd Ident Num text present")
public String EnterRegdIdentNumTextPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",3, 1))).getText();
}

@Step("click Regd Ident Num text box and send text")
public void EnterRegdIdentNumTextboxClick(int row) throws InterruptedException, IOException {
    WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",4, 1)));
    textbox.click();
    System.out.println(takeData.readExcelFile("testData",row, 7));
    textbox.sendKeys(takeData.readExcelFile("testData",row, 7));
}

@Step("clear Regd Ident Send Text")
public void ClearRegdIdentNumSendText() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",4, 1))).clear();
}



@Step("Enter Regd Ident Num validation")
public String EnterRegdIdentNumTextValidSec() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",5, 1))).getText();
}

@Step("Name of employer text present")
public String NameOfEmpTextPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",6, 1))).getText();
}

@Step("click Name of employer text box and send text")
public void EnterNameOfEmpTextboxClick(int row) throws InterruptedException, IOException {
    WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",7, 1)));
    textbox.click();
    System.out.println(takeData.readExcelFile("testData",row, 7));
    textbox.sendKeys(takeData.readExcelFile("testData",row, 7));
}

@Step("clear Name of employer Text")
public void ClearEnterNameOfEmpText() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",7, 1))).clear();
}



@Step("Enter Name of employervalidation")
public String EnterNameOfEmpTextValidSec() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",8, 1))).getText();
}

@Step("Registration Details text present")
public String RegistrationDetailsTextPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",9, 1))).getText();
}

@Step("Contact Number text present")
public String ContactNumberPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",10, 1))).getText();
}

@Step("click Contact Number text box and send text")
public void EnterContactNumberTextboxClick(int row) throws InterruptedException, IOException {
    WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",11, 1)));
    textbox.click();
    System.out.println(takeData.readExcelFile("testData",row, 7));
    textbox.sendKeys(takeData.readExcelFile("testData",row, 7));
}

@Step("clear Contact Number Send Text")
public void ClearContactNumberendText() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",11, 1))).clear();
}



@Step("Enter Contact Number validation")
public String ContactNumberValidSec() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",12, 1))).getText();
}

@Step("Email text present")
public String EmailPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",13, 1))).getText();
}

@Step("click Email text box and send text")
public void EnterEmailTextboxClick(int row) throws InterruptedException, IOException {
    WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",14, 1)));
    textbox.click();
    System.out.println(takeData.readExcelFile("testData",row, 7));
    textbox.sendKeys(takeData.readExcelFile("testData",row, 7));
}

@Step("clear Email Send Text")
public void ClearEmailendText() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",14, 1))).clear();
}



@Step("Enter Email validation1")
public String EmailValidSec1() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",15, 1))).getText();
}

@Step("Enter Email validation2")
public String EmailValidSec2() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",16, 1))).getText();
}

@Step("Get OTP Button present")
public String GetOTPButtonText() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",17, 1))).getText();
}

@Step("Get OTP Button enabled")
public boolean GetOTPButtonEnabled() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",17, 1))).isEnabled();
}

@Step("Registration validity upto present")
public String RegistrationvalidityuptoPresent() throws InterruptedException, IOException {
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",18, 1))).getText();
}

@Step("Select date from the calender")
public boolean selectDateFromCalender() throws InterruptedException, IOException {
    driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",19, 1))).click();
    Thread.sleep(10000);
    LocalDate currentdate = LocalDate.now();
    int currentDay = currentdate.getDayOfMonth();Month currentMonth = currentdate.getMonth();
    String monthName = currentMonth.toString().substring(0, 1).toUpperCase()+currentMonth.toString().substring(1).toLowerCase();
    //driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",20, 1)+currentDay+takeData.readExcelFile("FE_Registeration",21, 1)+monthName+takeData.readExcelFile("FE_Registeration",24, 1))).click();
    Thread.sleep(10000);
    return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registeration",20, 1)+currentDay+takeData.readExcelFile("FE_Registeration",21, 1)+monthName+takeData.readExcelFile("FE_Registeration",24, 1))).isDisplayed();     
     
}

}

