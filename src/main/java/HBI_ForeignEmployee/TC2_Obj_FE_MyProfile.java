package HBI_ForeignEmployee;

import java.io.IOException;
import org.openqa.selenium.By;
import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC2_Obj_FE_MyProfile extends MainBaseFeature{    



   ReadExcel takeData = new ReadExcel();
    
    
    @Step("Click on My Profile Button")
    public void clickOnMyProfile() throws InterruptedException, IOException {   
        driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",1, 1))).click();
        Thread.sleep(10000);
    }



   @Step("Change password Button Enabled")
    public boolean changePasswordButton() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",2, 1))).isEnabled();
    }
    
    @Step("Edit Profile Button Enabled")
    public boolean editProfileButton() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",3, 1))).isEnabled();
    }
    
    @Step("Registration Number section displayed")
    public boolean regdNumberPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",4, 1))).isDisplayed();
    }



   @Step("Registration Number section value displayed")
    public boolean regdNumberValuePresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",5, 1))).isDisplayed();
    }
    
    @Step("Registration Contact Number section displayed")
    public boolean regdContactPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",6, 1))).isDisplayed();
    }



   @Step("Registration  Contact Number section value displayed")
    public boolean regdContactNumberValuePresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",7, 1))).isDisplayed();
    }
    
    @Step("Registered Email section displayed")
    public boolean regEmailPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",8, 1))).isDisplayed();
    }



   @Step("Registered Email section email displayed")
    public boolean regdEmailValuePresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_MyProfile",9, 1))).isDisplayed();
    }
    
}
