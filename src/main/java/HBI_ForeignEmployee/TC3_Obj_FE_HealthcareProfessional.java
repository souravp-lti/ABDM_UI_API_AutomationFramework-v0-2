package HBI_ForeignEmployee;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;



import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC3_Obj_FE_HealthcareProfessional extends MainBaseFeature{    



   ReadExcel takeData = new ReadExcel();
    
    
    @Step("Click on Healthcare Professional tab")
    public void clickOnHealthcareProfessional() throws InterruptedException, IOException {   
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",1, 1))).click();
        Thread.sleep(10000);
    }



   @Step("Searchbox present")
    public boolean SearchBtnPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",2, 1))).isEnabled();
    }
    
    @Step("Enter value in searchbox and check Connect Button enabled")
    public boolean EnterValInSearchConnectBtn() throws InterruptedException, IOException {
        WebElement seachBtn = driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",2, 1)));
        seachBtn.sendKeys(takeData.readExcelFile("testData",1, 4));
        seachBtn.sendKeys(Keys.ENTER);
        
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",4, 1))).isEnabled();
    }
    
    @Step("Country drop down present")
    public boolean CountryDDPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",5, 1))).isDisplayed();
    }



   @Step("Enter value in Country Search box")
    public void enterCountryValInDD() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",5, 1))).click();
        Thread.sleep(10000);
        WebElement CountrySearchBtn = driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",6, 1)));
        String countryName = takeData.readExcelFile("testData",2, 4);
        System.out.println(countryName);
        CountrySearchBtn.click();
        Thread.sleep(10000);
        CountrySearchBtn.sendKeys(countryName);
        Thread.sleep(10000);
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",7, 1)+countryName+takeData.readExcelFile("FE_HealthcareProf",8, 1))).click();
        //driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",13, 1))).click(); //CLICK IN BLANK
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",7, 1)+countryName+takeData.readExcelFile("FE_HealthcareProf",8, 1))).sendKeys(Keys.ESCAPE);
        Thread.sleep(10000);
    }
    
    @Step("Type drop down present")
    public boolean TypeDDPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",9, 1))).isDisplayed();
    }



   @Step("Enter value in type Search box")
    public void enterTypeValInDD() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",9, 1))).click();
        Thread.sleep(10000);
        WebElement TypeSearchBtn = driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",10, 1)));
        String TypeName = takeData.readExcelFile("testData",3, 4);
        System.out.println(TypeName);
        TypeSearchBtn.click();
        Thread.sleep(10000);
        TypeSearchBtn.sendKeys(TypeName);
        Thread.sleep(10000);
        driver.findElement(By.xpath(takeData.readExcelFile("FE_HealthcareProf",11, 1)+TypeName+takeData.readExcelFile("FE_HealthcareProf",12, 1))).click();
        Thread.sleep(10000);
    }
    
    
}
