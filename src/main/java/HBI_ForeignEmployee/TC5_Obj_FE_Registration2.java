package HBI_ForeignEmployee;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC5_Obj_FE_Registration2 extends MainBaseFeature{    



   ReadExcel takeData = new ReadExcel();
    JavascriptExecutor js = (JavascriptExecutor) driver;
    
    @Step("Address text present")
    public String AddressTextPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",1, 1))).getText();
    }
    
    @Step("Country text present")
    public String CountryTextPresent() throws InterruptedException, IOException {
        //js.executeScript("window.scrollBy(0,-300)", "");
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",2, 1))).getText();
    }
    
    @Step("Click on Country Drop Down present")
    public void ClickCountryDropDown() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",3, 1))).click();
    }
    
    @Step("Click on Country Value from dropdown")
    public void SelValueCountryDropDown() throws InterruptedException, IOException {
        String country = takeData.readExcelFile("FE_Registration2",4, 1)+takeData.readExcelFile("testData",1, 10)+takeData.readExcelFile("FE_Registration2",5, 1);
        driver.findElement(By.xpath(country)).click();
    }
    
    @Step("State text present")
    public String StateTextPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",6, 1))).getText();
    }
    
    @Step("click State and send text")
    public void EnterStateTextboxClick(int row) throws InterruptedException, IOException {
        WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",7, 1)));
        textbox.click();
        System.out.println(takeData.readExcelFile("testData",row, 10));
        textbox.sendKeys(takeData.readExcelFile("testData",row, 10));
    }
    
    @Step("clear State Text")
    public void ClearStateText() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",7, 1))).clear();
    }



   @Step("State validation")
    public String StateValidSec() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",8, 1))).getText();
    }
    
    @Step("City/Town text present")
    public String CityTextPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",9, 1))).getText();
    }
    
    @Step("click City text box and send text")
    public void EnterCityTextboxClick(int row) throws InterruptedException, IOException {     
        WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",10, 1)));
        textbox.click();
        System.out.println(takeData.readExcelFile("testData",row, 10));
        textbox.sendKeys(takeData.readExcelFile("testData",row, 10));
    }
    
    @Step("clearCity Text")
    public void ClearCityText() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",10, 1))).clear();
    }



   @Step("City validation")
    public String CityValidSec() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",11, 1))).getText();
    }
    
    @Step("AddressSub text present")
    public String AddressSubTextPresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",12, 1))).getText();
    }
    
    @Step("click AddressSub text box and send text")
    public void EnterAddressSubClick(int row) throws InterruptedException, IOException {     
        WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",13, 1)));
        textbox.click();
        System.out.println(takeData.readExcelFile("testData",row, 10));
        textbox.sendKeys(takeData.readExcelFile("testData",row, 10));
    }
    
    @Step("clear AddressSub Text")
    public void ClearAddressSubText() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",13, 1))).clear();
    }



   @Step("AddressSub validation")
    public String AddressSubValidSec() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",14, 1))).getText();
    }
    
    @Step("Pincode text present")
    public String PincodePresent() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",15, 1))).getText();
    }
    
    @Step("click Pincode text box and send text")
    public void EnterPincodeClick(int row) throws InterruptedException, IOException {     
        WebElement textbox = driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",16, 1)));
        textbox.click();
        System.out.println(takeData.readExcelFile("testData",row, 10));
        textbox.sendKeys(takeData.readExcelFile("testData",row, 10));
    }
    
    @Step("clear Pincode Text")
    public void ClearPincodeText() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",16, 1))).clear();
    }



   @Step("Pincode validation")
    public String PincodeValidSec() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("FE_Registration2",17, 1))).getText();
    }
//----------------------------------------------------------------------------------------------------------------------
    
    
}
