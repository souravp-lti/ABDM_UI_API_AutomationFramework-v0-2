package Utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.mail.Store;

import org.testng.annotations.Test;

import com.testing.framework.EmailUtils;

import Base.MainBaseFeature;

public class ReadEmail extends MainBaseFeature
{
	@Test
	public void emailOtp() throws Exception
	{
		EmailUtils email = new EmailUtils();
		
		Properties prop = new Properties();
		FileInputStream file = new FileInputStream("C:\\Users\\10711204\\Desktop\\Demo\\ABDM_UI_API_AutomationFramework-v0-2\\config1.properties");
		prop.load(file);
		
	    Store connection = email.connectToGmail(prop);
	    List<String> emailText = email.getUnreadMessageByFromEmail(connection, "Inbox", "noreply.healthid@nha.gov.in", "Greetings From National Health Authority");
	    if(emailText.size()<1)
	    {
	    	throw new Exception("No OTP Received");
	    }
	    else 
	    {
	    	String regex = "[^\\d]+";
	    	String[] otp = emailText.get(0).split(regex);
	    	System.out.println("Received Otp is " +otp[1]);
	    }
	}
	
}
