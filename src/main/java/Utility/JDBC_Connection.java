package Utility;

import java.util.Properties;

import org.apache.poi.EncryptedDocumentException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
 
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import Base.MainBaseFeature;

public class JDBC_Connection extends MainBaseFeature
{
	private static void doSshTunnel( String strSshUser, String strSshPassword, String strSshHost, int nSshPort, String strRemoteHost, int nLocalPort, int nRemotePort ) throws JSchException
	  {
	    final JSch jsch = new JSch();
	    Session session = jsch.getSession( strSshUser, strSshHost, 22 );
	    session.setPassword( strSshPassword );
	     
	    final Properties config = new Properties();
	    config.put( "StrictHostKeyChecking", "no" );
	    session.setConfig( config );
	     
	    session.connect();
	    session.setPortForwardingL(nLocalPort, strRemoteHost, nRemotePort);
	  }
	   
//	  public static void main(String[] args)  throws EncryptedDocumentException, IOException, JSchException, SQLException
	  public static String data_fetch(String que, String dbname) throws Exception
	  {
		 //try
		  //{ 
		  String strSshUser = ReadExcel.readExcelFile("JDBC_Connection", 2, 1);      // SSH loging username
		  String strSshPassword = ReadExcel.readExcelFile("JDBC_Connection", 3, 1);  // SSH login password
		  String strSshHost = ReadExcel.readExcelFile("JDBC_Connection", 4, 1);;     // hostname or ip or SSH server
		  int nSshPort = 22;                                                         // remote SSH host port number
		  String strRemoteHost = ReadExcel.readExcelFile("JDBC_Connection", 6, 1);   // hostname or ip of your database server
		  int nLocalPort = 3366;                                                     // local port number use to bind SSH tunnel
		  int nRemotePort = 5022;                                                    // remote port number of your database 
		  String strDbUser = ReadExcel.readExcelFile("JDBC_Connection", 9, 1);       // database login username
		  String strDbPassword = ReadExcel.readExcelFile("JDBC_Connection", 10, 1);  // database login password
	   
	      JDBC_Connection.doSshTunnel(strSshUser, strSshPassword, strSshHost, nSshPort, strRemoteHost, nLocalPort, nRemotePort);
	       
	     // String dbname = "lgd";
	      
	      Connection con = DriverManager.getConnection("jdbc:postgresql://localhost:3366/"+dbname, strDbUser, strDbPassword);
	    
	      Statement st = con.createStatement();
	      
	      ResultSet rs = st.executeQuery(que);
	      
		 // Statement st = con.createStatement();
	      while (rs.next()) 
	      {
	          System.out.println(rs.getString(1));
	      }
	      
	      con.close();
	      /*
		  }
		  
		  catch( Exception e )
		    {
		      e.printStackTrace();
		    }
		    finally
		    {
		      System.exit(0);
		    }	
		    */
		return rs.getString(1);  
		  }

}
