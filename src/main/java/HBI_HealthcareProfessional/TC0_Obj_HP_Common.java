package HBI_HealthcareProfessional;

import java.io.IOException;
import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC0_Obj_HP_Common extends MainBaseFeature{
    



   ReadExcel takeData = new ReadExcel();
    
    
    @Step("Login/Registration button")
    public void clickLoginRegisterationButton() throws InterruptedException, IOException {    
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath(takeData.readExcelFile("FE_Common",2, 1))).click();
    }
    
    @Step("HealthCare Professional option")
    public void clickHPButton() throws InterruptedException, IOException {    
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",1, 1))).click();
    }
    
    @Step("Registration button")
    public void clickRegistrationButton() throws InterruptedException, IOException {    
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",2, 1))).click();
    }

   
}


