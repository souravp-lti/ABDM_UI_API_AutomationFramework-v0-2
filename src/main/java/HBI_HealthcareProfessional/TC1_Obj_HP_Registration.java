package HBI_HealthcareProfessional;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;



import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;



import Base.MainBaseFeature;
import Utility.ReadExcel;
import io.qameta.allure.Step;




public class TC1_Obj_HP_Registration extends MainBaseFeature{
    



   ReadExcel takeData = new ReadExcel();
    JavascriptExecutor js = (JavascriptExecutor) driver;
    
    
    @Step("Switch to HPR reg page")
    public void SwitchToHPRWindow() throws InterruptedException, IOException {    
        // It will return the parent window name as a String
        String parent=driver.getWindowHandle();
        Set<String>s=driver.getWindowHandles();
        // Now iterate using Iterator
        Iterator<String> I1= s.iterator();
        while(I1.hasNext())
        {
        String child_window=I1.next();
        if(!parent.equals(child_window))
        {
        driver.switchTo().window(child_window);
        System.out.println(driver.switchTo().window(child_window).getTitle());
        //driver.close();
        }
        }
        //switch to the parent window
        //driver.switchTo().window(parent);    
        //Thread.sleep(10000);
    }
    
    @Step("Click on Register Now button in HPR page")
    public void ClickOnRegisterNowButton() throws InterruptedException, IOException {    
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",3, 1))).click();
    }
    
    @Step("Welcome Image")
    public String WelcomeImgText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",4, 1))).getText();
    }
    
    @Step("To Ayushman Bharat Digital Mission text")
    public String ABDMText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",5, 1))).getText();
    }
    
    @Step("Healthcare Professional Registry text")
    public String HPRText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",6, 1))).getText();
    }
    
    @Step("Select your registration council text")
    public String SelurRegdConText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",7, 1))).getText();
    }
    
    @Step("Enter registration number text")
    public String EntRegdNumText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",8, 1))).getText();
    }
    
    @Step("Select Category text")
    public String SelCateText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",9, 1))).getText();
    }
    
    @Step("Select Sub Category  text")
    public String SelSubCateText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",10, 1))).getText();
    }
    
    @Step("Submit Button is present")
    public boolean SubmitBtnVisible() throws InterruptedException, IOException {  
        js.executeScript("window.scrollBy(0,300)", "");
        Thread.sleep(5000);
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",11, 1))).isDisplayed();
    }
    
    @Step("Submit Button text")
    public String SubmitText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",11, 1))).getText();
    }
    
    @Step("Submit Button is clicked")
    public void SubmitBtnClick() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",11, 1))).click();
        js.executeScript("window.scrollBy(0,-300)", "");
    }
    
    @Step("Reset Button is present")
    public boolean ResetBtnVisible() throws InterruptedException, IOException {  
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",12, 1))).isDisplayed();
    }
    
    @Step("Reset Button text")
    public String ResetText() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",12, 1))).getText();
    }
    
    @Step("Reset Button is clicked")
    public void ResetBtnClick() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",12, 1))).click();
        Thread.sleep(5000);
        js.executeScript("window.scrollBy(0,-300)", "");
    }
    
    @Step("Select your registration council not selected validation")
    public String CouncilVal() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",13, 1))).getText();
    }
    
    @Step("Enter registration number not selected validation")
    public String RegdNumVal() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",14, 1))).getText();
    }
    
    @Step("Category not selected validation")
    public String CateVal() throws InterruptedException, IOException {
        js.executeScript("window.scrollBy(0,300)", "");
        Thread.sleep(5000);
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",15, 1))).getText();
    }
    
    @Step("Category not selected validation")
    public String SubCateVal() throws InterruptedException, IOException {
        return driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",16, 1))).getText();
    }
    
    @Step("Click on Select your registration council drop down")
    public void ClickRegdCouncildropDown() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",17, 1))).click();
    }
    
    @Step("Click on Category drop down")
    public void CategoryropDown() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",22, 1))).click();
        js.executeScript("window.scrollBy(0,300)", "");
    }
    
    @Step("Click on Sub Category drop down")
    public void SubCatedropDown() throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",23, 1))).click();
    }
    
    @Step("Click on Select your registration council search")
    public void SearchValdropDown(int i) throws InterruptedException, IOException {
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",18, 1))).click();
        Thread.sleep(5000);
        driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",18, 1))).sendKeys(takeData.readExcelFile("testData_2",i, 1));
        Thread.sleep(5000);
    }
    
    @Step("Select value from Select your registration council drop down")
    public void SelValueListdropDown(int i) throws InterruptedException, IOException {
        String regdCouncil = takeData.readExcelFile("HP_Registration",19, 1)+takeData.readExcelFile("testData_2",i, 1)+takeData.readExcelFile("HP_Registration",20, 1);
        System.out.println(regdCouncil);
        driver.findElement(By.xpath(regdCouncil)).click();
        //driver.findElement(By.xpath(regdCouncil)).sendKeys(Keys.ESCAPE);
    }
    
    @Step("Enter text in Enter Registration number")
    public void EnterRegdNum() throws InterruptedException, IOException {
        WebElement seachBtn = driver.findElement(By.xpath(takeData.readExcelFile("HP_Registration",21, 1)));
        seachBtn.sendKeys(takeData.readExcelFile("testData_2",2, 1));
    }
    
}
