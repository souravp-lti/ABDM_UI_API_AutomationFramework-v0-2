package HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class LoginPage extends MainBaseFeature
{
    JavascriptExecutor js = (JavascriptExecutor)driver;
    WebDriverWait wait = new WebDriverWait(driver, 20);
    
	
	public String loginToHBIforHP() throws InterruptedException, IOException 
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));
		loginRegistrationBtn.click();
		Thread.sleep(2000);
		WebElement healthcareProfessionalOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 4, 1)));
		healthcareProfessionalOpt.click();
		Thread.sleep(2000);
		WebElement loginBtn                  = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn.click();
		Thread.sleep(2000);
		WebElement hpidTextBox               = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 6, 1)));
		hpidTextBox.clear();
		hpidTextBox.sendKeys(ReadExcel.readExcelFile("testData", 6, 3));	
		Thread.sleep(2000);
		WebElement submitBtn                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 7, 1)));
		submitBtn.click();
		Thread.sleep(2000);
		WebElement loginWithPasswordBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 8, 1)));
		loginWithPasswordBtn.click();
		Thread.sleep(2000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 9, 1)));
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 3));
		Thread.sleep(2000);
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 10, 1)));
		loginBtn2.click();
//		WebElement drName                    = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 11, 1)));
		WebElement logoutBtn                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 12, 1)));
		WebElement profile                   = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 13, 1)));
		Thread.sleep(2000);
//		System.out.println("Profile = " +profile.getText());
		return profile.getText();    
	}
	
	public String loginToHBIforNO() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));		
		loginRegistrationBtn.click();
		Thread.sleep(2000);
		WebElement nodalOfficerOpt           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 17, 1)));
		nodalOfficerOpt.click();
		Thread.sleep(2000);
		WebElement loginBtn1                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn1.click();
		Thread.sleep(2000);
		WebElement emailIDTextbox            = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 18, 1)));
		emailIDTextbox.clear();
		emailIDTextbox.sendKeys(ReadExcel.readExcelFile("testData", 6, 1));
		Thread.sleep(2000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 19, 1)));
		passwordTextBox.clear();
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 1));
		Thread.sleep(2000);
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 20, 1)));
		loginBtn2.click();
		Thread.sleep(2000);
		WebElement profileName               = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 21, 1)));
		
		String userName = profileName.getText();
		return userName;
		
	}
	
	public String loginToHbiForRA() throws InterruptedException, EncryptedDocumentException, IOException
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));		
		loginRegistrationBtn.click();
		Thread.sleep(2000);
		WebElement recruitementAgentOpt      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 28, 1)));
		recruitementAgentOpt.click();
		Thread.sleep(2000);
		WebElement loginBtn1                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn1.click();
		Thread.sleep(2000);
		WebElement emailIDTextbox            = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 18, 1)));
		emailIDTextbox.clear();
		emailIDTextbox.sendKeys(ReadExcel.readExcelFile("testData", 6, 2));
		Thread.sleep(2000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 19, 1)));
		passwordTextBox.clear();
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 2));
		Thread.sleep(2000);
		
//--------------------Captcha-------------------------------------------------------
		
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[starts-with(@name, 'a-') and starts-with(@src, 'https://www.google.com/recaptcha')]")));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='rc-anchor-container']//div[@class='recaptcha-checkbox-border']"))).click();
		
//		new WebDriverWait(driver, 10).until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(By.xpath("//iframe[starts-with(@name, 'a-') and starts-with(@src, 'https://www.google.com/recaptcha')]"))); 
//
//		new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div.recaptcha-checkbox-checkmark"))).click(); 

//		WebElement iFrame = driver.findElement(By.xpath("//iframe[@title='reCAPTCHA']"));
//		driver.switchTo().frame(iFrame);
//		Thread.sleep(2000);
//		
//		WebElement captchaCheckbox = driver.findElement(By.xpath("//div[@id='rc-anchor-container']//div[@class='recaptcha-checkbox-border']"));
//		captchaCheckbox.click();
//		Thread.sleep(2000);
		
		js.executeScript("window.scrollBy(0,200)", "");
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 20, 1)));
		loginBtn2.click();
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 29, 1)));
		String profileName = userName.getText();
		return profileName;
	}
	
	public void loginNodalOfficer() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));		
		loginRegistrationBtn.click();
//		Thread.sleep(2000);
		WebElement nodalOfficerOpt           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 17, 1)));
		nodalOfficerOpt.click();
//		Thread.sleep(2000);
		WebElement loginBtn1                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn1.click();
//		Thread.sleep(2000);
		WebElement emailIDTextbox            = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 18, 1)));
		emailIDTextbox.clear();
		emailIDTextbox.sendKeys(ReadExcel.readExcelFile("testData", 6, 1));
//		Thread.sleep(2000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 19, 1)));
		passwordTextBox.clear();
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 1));
//		Thread.sleep(2000);
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 20, 1)));
		loginBtn2.click();
		Thread.sleep(2000);
	}
	
	public void loginHealthcareProfessional() throws InterruptedException, EncryptedDocumentException, IOException
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));
		loginRegistrationBtn.click();
//		Thread.sleep(2000);
		WebElement healthcareProfessionalOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 4, 1)));
		healthcareProfessionalOpt.click();
//		Thread.sleep(2000);
		WebElement loginBtn                  = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn.click();
//		Thread.sleep(2000);
		WebElement hpidTextBox               = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 6, 1)));
		hpidTextBox.clear();
		hpidTextBox.sendKeys(ReadExcel.readExcelFile("testData", 6, 3));	
//		Thread.sleep(2000);
		WebElement submitBtn                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 7, 1)));
		submitBtn.click();
//		Thread.sleep(2000);
		WebElement loginWithPasswordBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 8, 1)));
		loginWithPasswordBtn.click();
//		Thread.sleep(2000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 9, 1)));
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 3));
//		Thread.sleep(2000); 
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 10, 1)));
		loginBtn2.click();
		Thread.sleep(2000);
	}
	
	public void loginRecruitementAgent() throws InterruptedException, EncryptedDocumentException, IOException
	{
		WebElement loginRegistrationBtn      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 3, 1)));		
		loginRegistrationBtn.click();
		Thread.sleep(1000);
		WebElement recruitementAgentOpt      = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 28, 1)));
		recruitementAgentOpt.click();
		Thread.sleep(1000);
		WebElement loginBtn1                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 5, 1)));
		loginBtn1.click();
		Thread.sleep(1000);
		WebElement emailIDTextbox            = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 18, 1)));
		emailIDTextbox.clear();
		emailIDTextbox.sendKeys(ReadExcel.readExcelFile("testData", 6, 2));
		Thread.sleep(1000);
		WebElement passwordTextBox           = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 19, 1)));
		passwordTextBox.clear();
		passwordTextBox.sendKeys(ReadExcel.readExcelFile("testData", 7, 2));
		Thread.sleep(1000);
		WebElement loginBtn2                 = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_LoginPage", 20, 1)));
		loginBtn2.click();
		Thread.sleep(1000);
	}

}
