package HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class HP_LandingPage extends MainBaseFeature
{
	JavascriptExecutor js = (JavascriptExecutor)driver;
	Actions act =  new Actions(driver);
	
	// All the Tiles Present on the Dashboard Page 
	public boolean initiatedTile() throws EncryptedDocumentException, IOException
	{
		WebElement initiatedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 4, 1)));
		return initiatedTile.isDisplayed();
	}
	
	public boolean employerConnectedTile() throws EncryptedDocumentException, IOException
	{
		WebElement employerConnectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 5, 1)));
	    return employerConnectedTile.isDisplayed();
	}
	
	public boolean verified() throws EncryptedDocumentException, IOException
	{
		WebElement verifiedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 6, 1)));
	    return verifiedTile.isDisplayed();
	}
	
	public boolean preDepartureOrientationTile() throws EncryptedDocumentException, IOException
	{
		WebElement preDepartureOrientation = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 7, 1)));
	    return preDepartureOrientation.isDisplayed();
	}
	
	public boolean visaOrWorkPermitProcessingTile() throws EncryptedDocumentException, IOException
	{
		WebElement visaOrWorkPermitProcessing = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 8, 1)));
	    return visaOrWorkPermitProcessing.isDisplayed();
	}
	
	public boolean readyForDepartureTile() throws EncryptedDocumentException, IOException
	{
		WebElement readyForDeparture = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 9, 1)));
	    return readyForDeparture.isDisplayed();
	}
	
	public boolean employmentStartedTile() throws EncryptedDocumentException, IOException
	{
		WebElement readyForDeparture = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 10, 1)));
	    return readyForDeparture.isDisplayed();
	}
	
	public boolean withdrawnRejectedTile() throws EncryptedDocumentException, IOException
	{
		WebElement withdrawnRejectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 11, 1)));
	    return withdrawnRejectedTile.isDisplayed();
	}
	
	//Change Password on My Profile Page 
	public String changePassword() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement myProfileOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 27, 1)));
		Thread.sleep(2000);
		myProfileOpt.click();
		
		WebElement changePasswordBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 28, 1)));
		changePasswordBtn.click();
		WebElement oldPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 29, 1)));
		oldPassword.sendKeys("Hp@id&03");
		WebElement newPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 30, 1)));
        newPassword.sendKeys("Hp@id&04");
        WebElement confirmPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 31, 1)));
        confirmPassword.sendKeys("Hp@id&04");
        Thread.sleep(2000);
        WebElement submitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HP_LandingPage", 32, 1)));
        js.executeScript("arguments[0].click();", submitBtn);
        Thread.sleep(3000);
 //     submitBtn.click();
        
        Alert alt = driver.switchTo().alert();
        String sucessfulMessage = alt.getText();
        alt.accept();
        System.out.println("Alert Message = " +sucessfulMessage);
        return sucessfulMessage;
        
	}
	
	

}
