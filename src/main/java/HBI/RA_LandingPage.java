package HBI;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class RA_LandingPage extends MainBaseFeature
{
	JavascriptExecutor js = (JavascriptExecutor)driver;
	Actions act =  new Actions(driver);
	
	 //Checking Initiated tile is present and is clickable and on click its shows the data of that status	
		public String initiatedTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			Thread.sleep(1000);
			WebElement initiatedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 4, 1)));
			Thread.sleep(1000);
			js.executeScript("arguments[0].click();", initiatedTile);
//			initiatedTile.click();
			Thread.sleep(1000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 5, 1)));
			return dataStatus.getText();
		}
		
	// Checking Employer Connected tile is present and is clickable and on click its shows the data of that status
		public String employerConnectedTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			Thread.sleep(1000);
			WebElement employerConnectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 6, 1)));
			js.executeScript("arguments[0].click();", employerConnectedTile);
//			employerConnectedTile.click();
			Thread.sleep(1000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 7, 1)));
			Thread.sleep(1000);

			return dataStatus.getText();
		}
		
		public String verifiedTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement verifiedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 8, 1)));
			verifiedTile.click();
			Thread.sleep(2000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 9, 1)));
			
			return dataStatus.getText();
		}
		
		public String preDepartureOrientationTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			Thread.sleep(1000);
			WebElement dataCount = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 52, 1)));
			int count = Integer.parseInt(dataCount.getText());
			
			if(count==0)
			{
				WebElement preDepartureOrientationTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 10, 1)));
				js.executeScript("arguments[0].click();", preDepartureOrientationTile);
				Thread.sleep(1000);
			    WebElement noResultFoundLabel = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 53, 1)));
			    Thread.sleep(1000);
			    return noResultFoundLabel.getText();  
			}
			else
			{
				WebElement preDepartureOrientationTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 10, 1)));
				js.executeScript("arguments[0].click();", preDepartureOrientationTile);
				Thread.sleep(1000);
				WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 10, 3)));
				Thread.sleep(1000);
				return dataStatus.getText();

			}
				
		}
		
		public String visaWorkPermitProcessingTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement visaWorkPermitProcessingTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 11, 1)));
			visaWorkPermitProcessingTile.click();
			Thread.sleep(2000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 12, 1)));
			return dataStatus.getText();
		}
		
		public String readyForDepartureTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement readyForDepartureTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 13, 1)));
			readyForDepartureTile.click();
			Thread.sleep(2000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 14, 1)));
			
			return dataStatus.getText();
		}
		
		public String employementStartedTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement employementStartedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 15, 1)));
			employementStartedTile.click();
			Thread.sleep(2000);
			WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 16, 1)));
			
			return dataStatus.getText();
		}
		
		public boolean WithdrawnRejectedTile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement WithdrawnRejectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 17, 1)));
			
			return WithdrawnRejectedTile.isDisplayed();
		}
		
		public boolean healthcareProfessionalSearchBox() throws InterruptedException, EncryptedDocumentException, IOException
		{
			WebElement healthcareProfessionalOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 8, 1)));
			healthcareProfessionalOpt.click();
			Thread.sleep(2000);
			WebElement searchBox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 9, 1)));
			searchBox.click();
			Thread.sleep(2000);
			searchBox.sendKeys("Madhura");
			Thread.sleep(2000);
			searchBox.sendKeys(Keys.ENTER);
			Thread.sleep(5000);
			
			WebElement resultContent = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 10, 1)));
			return resultContent.isDisplayed();	
		}

// Edit Profile Section
// Change Password Functionality		
		public String changePassword() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
	        myProfile.click();
			WebElement changePasswordBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 26, 1)));
            changePasswordBtn.click();
			WebElement newPasswordTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 27, 1)));
			newPasswordTextbox.sendKeys(ReadExcel.readExcelFile("testData", 3, 6));
			Thread.sleep(1000);
			WebElement confirmNewPasswordBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 28, 1)));
			confirmNewPasswordBtn.sendKeys(ReadExcel.readExcelFile("testData", 3, 6));
			Thread.sleep(1000);
			WebElement submitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 29, 1)));
            submitBtn.click();
            Thread.sleep(3000);
              
        
            WebDriverWait wait = new WebDriverWait(driver, 3);
            WebElement okBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 30, 1)));
            wait.until(ExpectedConditions.elementToBeClickable(okBtn));
	    	WebElement acknowledgementText = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
//	   	    okBtn.click();
		    System.out.println("---------------------" +acknowledgementText.getText());
		    return acknowledgementText.getText();
          		                             
         }	
		
//Edit Profile Page 
		//Enter Registration Number Textbox
		public boolean enterRegistrationNumberTextbox() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
            editProfile.click();
            Thread.sleep(2000);
	    	WebElement enterRegNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 37, 1)));
            return enterRegNumberTextbox.isEnabled();
		}
		
		//Name of Recruiting Agency Textbox
		public String nameOfRecruitingAgencyTextbox() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
            editProfile.click();
            Thread.sleep(2000);
			WebElement nameOfRecruitingAgencyTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 38, 1)));
            System.out.println("----------"+nameOfRecruitingAgencyTextbox.getAttribute("value"));
			return nameOfRecruitingAgencyTextbox.getAttribute("value");
		}
		
		//Contact Number textbox
		public boolean contactNumberTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
            editProfile.click();
			WebElement contactNumber = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 39, 1)));
            return contactNumber.isEnabled();
		}
		
		//Email ID textbox
		public boolean emailIDTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
		    editProfile.click();
			WebElement emailID = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 40, 1)));
		    return emailID.isEnabled();
		}
		
		//Registration Valid Upto calender
		public boolean registrationvalidUpto() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
		    editProfile.click();
		    js.executeScript("window.scrollBy(0,300)", "");
			WebElement registrationValidUptoCalender = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 41, 1)));
            return registrationValidUptoCalender.isEnabled();
		}
		
	//Select the country recruiting for dropdown
		public boolean selectTheCountryRecruitingForDropdown() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,300)", "");
			WebElement selectCountryRecruitingFor = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 42, 1)));
			Thread.sleep(2000);
			return selectCountryRecruitingFor.isEnabled();
		}
		
	//Select the recruiting healthcare professional type dropdown	
		public boolean selectTheRecruitingHealthcareProfessionalTypeDropdown() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,300)", "");
			WebElement selectTheRecruitingHealthcareProfessionalType = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 43, 1)));
			Thread.sleep(2000);
			return selectTheRecruitingHealthcareProfessionalType.isEnabled();
		}
		
	//Country textbox under Agent Details on Edit Profile page 	
		public boolean countryTextbox() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,500)", "");
			WebElement countryTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 44, 1)));
			Thread.sleep(2000);
			return countryTextbox.isEnabled();
		}
		
//State Dropdown		
		public boolean stateDropdown() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,600)", "");
			WebElement stateDropdown = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 45, 1)));
			Thread.sleep(2000);
			return stateDropdown.isDisplayed();
		}

//City Dropdown		
		public boolean cityDropdown() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,600)", "");
			WebElement cityDropdown = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 46, 1)));
			Thread.sleep(2000);
			return cityDropdown.isDisplayed();
		}

//Address Textbox
		public boolean adressTextbox() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,600)", "");
			WebElement addressTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 47, 1)));
			Thread.sleep(2000);
			return addressTextbox.isEnabled();
		}

//Pincode textbox
		public boolean pincodeTextbox() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
			myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
			editProfile.click();
			js.executeScript("window.scrollBy(0,600)", "");
			WebElement pincodeTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 48, 1)));
			Thread.sleep(2000);
			return pincodeTextbox.isEnabled();
		}
		
//Edit profile complete flow	
		public String editProfile() throws InterruptedException, EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 25, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 36, 1)));
            editProfile.click();
            Thread.sleep(2000);
	    	WebElement enterRegNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 37, 1)));
			enterRegNumberTextbox.clear();
			Thread.sleep(1000);
			enterRegNumberTextbox.sendKeys(ReadExcel.readExcelFile("testData", 10, 1));
			
			WebElement nameOfRecruitingAgencyTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 38, 1)));
            nameOfRecruitingAgencyTextbox.clear();
			Thread.sleep(1000);
            nameOfRecruitingAgencyTextbox.sendKeys(ReadExcel.readExcelFile("testData", 11, 1));
            
            js.executeScript("window.scrollBy(0,400)", "");
			WebElement registrationValidUptoCalender = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 41, 1)));
            js.executeScript( "arguments[0].click();", registrationValidUptoCalender);
//			registrationValidUptoCalender.click();
			Thread.sleep(2000);
			WebElement date = driver.findElement(By.xpath(ReadExcel.readExcelFile("testData", 12, 1)));
            date.click();
			Thread.sleep(1000);
			
           js.executeScript("window.scrollBy(0,250)", "");
            WebElement stateDropdown = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 45, 1)));
//            js.executeScript("arguments[0].scrollIntoView();", stateDropdown);
//            js.executeScript( "arguments[0].click();", stateDropdown);
            stateDropdown.click();
            Thread.sleep(1000);
            List<WebElement>  listOfAllTheStates = driver.findElements(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 49, 1)));
            for(int i=1; i<listOfAllTheStates.size(); i++ )
            {
            	String need = "UTTAR PRADESH";
            	String actual = listOfAllTheStates.get(i).getText();
            	if(need.equals(actual))
            	{
            		listOfAllTheStates.get(i).click();
            		Thread.sleep(2000);
            		break;
            	}
            }
            
			WebElement cityDropdown = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 46, 1)));
//            js.executeScript("arguments[0].scrollIntoView();", cityDropdown);
//			js.executeScript( "arguments[0].click();", cityDropdown);
			cityDropdown.click();
			Thread.sleep(1000);
            List<WebElement>  listOfAllTheCities = driver.findElements(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 50, 1)));
            for(int i=1; i<=listOfAllTheCities.size(); i++ )
            {
            	String need = "BHADOHI";
            	String actual = listOfAllTheCities.get(i).getText();
            	if(need.equals(actual))
            	{
            		listOfAllTheCities.get(i).click();
            		Thread.sleep(2000);
            		break;
            	}
            }
            
			WebElement addressTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 47, 1)));
            addressTextbox.clear();
            Thread.sleep(1000);
            addressTextbox.sendKeys("Jagnade Chowk");
			Thread.sleep(2000);
            
			WebElement updateAndSubmitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 55, 1)));
			updateAndSubmitBtn.click();
			Thread.sleep(2000);
			
			WebDriverWait wait = new WebDriverWait(driver, 5);
            WebElement successfulMessage = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_RA_LandingPage", 56, 1)));
            wait.until(ExpectedConditions.elementToBeClickable(successfulMessage));
            
            return successfulMessage.getText();
            

            
            
			
		}
		

}
