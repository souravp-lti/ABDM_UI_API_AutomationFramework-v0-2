package HBI;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class NO_LandingPage extends MainBaseFeature
{
	JavascriptExecutor js = (JavascriptExecutor)driver;
	Actions act =  new Actions(driver);
	
	public String initiatedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement initiatedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 4, 2)));
		Thread.sleep(2000);
		initiatedTile.click();
		Thread.sleep(2000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 5, 1)));
		return dataStatus.getText();
	}
	
// Checking Employer Connected tile is present and is clickable and on click its shows the data of that status
	public String employerConnectedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement employerConnectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 6, 1)));
		employerConnectedTile.click();
		Thread.sleep(2000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 7, 1)));
		
		return dataStatus.getText();
	}
	
	public String verifiedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement verifiedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 8, 1)));
		verifiedTile.click();
		Thread.sleep(2000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 9, 1)));
		
		return dataStatus.getText();
	}
	
	public String preDepartureOrientationTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		Thread.sleep(1000);
		WebElement preDepartureOrientationTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 10, 1)));
		preDepartureOrientationTile.click();
		Thread.sleep(1000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 11, 1)));
		Thread.sleep(1000);

		return dataStatus.getText();
	}
	
	public String visaWorkPermitProcessingTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement visaWorkPermitProcessingTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 12, 1)));
		visaWorkPermitProcessingTile.click();
		Thread.sleep(2000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 13, 1)));
		return dataStatus.getText();
	}
	
	public String readyForDepartureTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		Thread.sleep(1000);
		WebElement readyForDepartureTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 14, 1)));
		readyForDepartureTile.click();
		Thread.sleep(1000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 15, 1)));
		Thread.sleep(1000);

		return dataStatus.getText();
	}
	
	public String employementStartedTile() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement employementStartedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 16, 1)));
		employementStartedTile.click();
		Thread.sleep(2000);
		WebElement dataStatus = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 17, 1)));
		Thread.sleep(2000);

		return dataStatus.getText();
	}
	
	public String searchFunctionalityOnDashboardForRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement userTypeDropdwon = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 22, 1)));
		userTypeDropdwon.click();
		Thread.sleep(2000);
		WebElement recruitementAgentOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 23, 2)));
		act.moveToElement(recruitementAgentOpt).click().perform();
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 25, 2)));
		searchBtn.click();
		WebElement type = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 26, 1)));
		type.getText();
		
		return type.getText();	
	}
	
	public String searchFunctionalityOnDashboardForFE() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement userTypeDropdwon = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 22, 1)));
		userTypeDropdwon.click();
		Thread.sleep(2000);
		WebElement foreignEmployerOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 24, 1)));
		act.moveToElement(foreignEmployerOpt).click().perform();
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 25, 1)));
		searchBtn.click();
		WebElement type = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 27, 1)));
		type.getText();
		
		return type.getText();	
	}
	
	public String userTypeFilterFunctionalityOnApprovalPendingPageForRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
	//	approvalPendingOpt.click();
		Thread.sleep(2000);
		WebElement userTypeDropdwon = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 22, 1)));
		userTypeDropdwon.click();
		Thread.sleep(2000);
		WebElement recruitementAgentOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 23, 1)));
		act.moveToElement(recruitementAgentOpt).click().perform();
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 25, 1)));
		searchBtn.click();
		WebElement type = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 26, 1)));
		type.getText();
		
		return type.getText();
	}
	
	public String userTypeFilterFunctionalityOnApprovalPendingPageForFE() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
	//	approvalPendingOpt.click();
		WebElement userTypeDropdwon = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 22, 1)));
		userTypeDropdwon.click();
		Thread.sleep(2000);
		WebElement foreignEmployerOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 24, 1)));
		act.moveToElement(foreignEmployerOpt).click().perform();
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 25, 1)));
		searchBtn.click();
		WebElement type = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 27, 1)));
		type.getText();
		
		return type.getText();	
	}
	
	public String userTypeFilterFunctionalityOnApprovalPendingPageForNO() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
	//	approvalPendingOpt.click();
		WebElement userTypeDropdwon = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 22, 1)));
		userTypeDropdwon.click();
		Thread.sleep(2000);
		WebElement nodalOfficerOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 32, 1)));
		act.moveToElement(nodalOfficerOpt).click().perform();
		Thread.sleep(2000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 25, 1)));
		searchBtn.click();
		WebElement type = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 33, 1)));
		type.getText();
		
		return type.getText();	
	}
	
	public boolean submittedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
		Thread.sleep(2000);
		WebElement submittedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 35, 1)));
		return submittedTile.isDisplayed();
	}
	
	public boolean approvedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
		Thread.sleep(2000);
		WebElement submittedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 36, 1)));
		return submittedTile.isDisplayed();
	}
	
	public boolean rejectedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
		Thread.sleep(2000);
		WebElement rejectedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 37, 1)));
		return rejectedTile.isDisplayed();
	}
	
	public boolean suspendedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
		Thread.sleep(2000);
		WebElement suspendedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 38, 1)));
		return suspendedTile.isDisplayed();
	}
	
	public boolean deactivatedTileOnApprovalPendingPage() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement approvalPendingOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 31, 1)));
		act.moveToElement(approvalPendingOpt).click().perform();
		Thread.sleep(2000);
		WebElement deactivatedTile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 39, 1)));
		return deactivatedTile.isDisplayed();
	}
	
// Edit Profile	section 
// name of Authorised Official textbox	
	public boolean nameOfAuthorisedOfficialTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
		myProfile.click(); 
		Thread.sleep(2000);
		WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
        editProfile.click();
    	Thread.sleep(2000);
		WebElement nameOfAuthorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 46, 1)));

        return  nameOfAuthorisedOfficialTextbox.isEnabled();    	
	}
	
// Authorised Official textbox	
		public boolean authorisedOfficialTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement authorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 47, 1)));

	        return  authorisedOfficialTextbox.isEnabled();    	
		}
	
// Designation textbox	
		public boolean designationTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement designationTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 48, 1)));

	        return  designationTextbox.isEnabled();    	
		}

// Contact Number textbox	
		public boolean contactNumberTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement contactNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 49, 1)));

	        return  contactNumberTextbox.isEnabled();    	
		}

// Email ID textbox		
		public boolean emailIDTextbox() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement emailIDTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 51, 1)));

	        return  emailIDTextbox.isEnabled();    	
		}
	
// Get Otp button			
		public boolean getOtpButton() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement getOtpButton = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 50, 1)));

	        return  getOtpButton.isEnabled();    	
		}

//Update/Submit button
		public boolean updateAndSubmitButton() throws EncryptedDocumentException, IOException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement editProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
	        editProfile.click();
			WebElement updateAndSubmitButton = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 53, 1)));

	        return  updateAndSubmitButton.isEnabled();    	
		}
		
		public String editProfile() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfileOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
		    myProfileOpt.click();
		   
		    WebElement editProfileBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 45, 1)));
		    editProfileBtn.click();
			
		    WebElement nameOfAuthorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 46, 1)));
            nameOfAuthorisedOfficialTextbox.clear();
            Thread.sleep(1000);
            nameOfAuthorisedOfficialTextbox.sendKeys("Chandra DV");
			
            WebElement authorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 47, 1)));
            authorisedOfficialTextbox.clear();
            Thread.sleep(1000);
            authorisedOfficialTextbox.sendKeys("Quality Analyst"); 
			
            WebElement designationTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 48, 1)));
            designationTextbox.clear();
            designationTextbox.sendKeys("ABC");
  
            WebElement updateAndSubmitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 53, 1)));
            js.executeScript("window.scrollBy(0,300)", "");
            Thread.sleep(2000);
            updateAndSubmitBtn.click();
            Thread.sleep(2000);

            
            WebDriverWait wait = new WebDriverWait(driver, 5);
            WebElement successfulMessage = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 54, 1)));
            wait.until(ExpectedConditions.elementToBeClickable(successfulMessage));
            
            return successfulMessage.getText();
		    
		}
		
//Change Password in my profile	
		public String changePassword() throws EncryptedDocumentException, IOException, InterruptedException
		{
			WebElement myProfile = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 44, 1)));
	        myProfile.click();
			WebElement changePasswordBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 55, 1)));
            changePasswordBtn.click();
			WebElement newPasswordTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 56, 1)));
			newPasswordTextbox.sendKeys(ReadExcel.readExcelFile("testData", 3, 5));
			WebElement confirmNewPasswordBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 57, 1)));
			confirmNewPasswordBtn.sendKeys(ReadExcel.readExcelFile("testData", 3, 5));
			WebElement submitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 58, 1)));
            submitBtn.click();
            Thread.sleep(5000);
              
        
            WebDriverWait wait = new WebDriverWait(driver, 10);
            WebElement okBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 60, 1)));
            wait.until(ExpectedConditions.elementToBeClickable(okBtn));
	    	WebElement acknowledgementText = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_NO_LandingPage", 59, 1)));
//	   	    okBtn.click();
		    System.out.println("---------------------" +acknowledgementText.getText());
		    return acknowledgementText.getText();
          		                             
         }	

}
