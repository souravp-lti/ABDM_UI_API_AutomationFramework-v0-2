package HBI;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class HomePage extends MainBaseFeature
{
	ArrayList<String> arr = new ArrayList<String>();
	Actions act = new Actions(driver);
	JavascriptExecutor js = (JavascriptExecutor)driver;
	
	public boolean mohfwLogo() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement mohfwLogo1                = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 1, 1)));
		Thread.sleep(2000);
		return mohfwLogo1.isDisplayed();
	}
	
	public boolean azadiLogo() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement azadiLogo = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 12, 1)));
		Thread.sleep(2000);
		boolean value = azadiLogo.isDisplayed();
		return value;
	}
	
	public boolean hbiLogo() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement hbiLogo   = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 43, 1)));
		Thread.sleep(2000);
		return hbiLogo.isDisplayed();
	}
	
	public boolean homeButton() throws EncryptedDocumentException, IOException
	{
		WebElement homeBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 15, 1)));
		boolean display = homeBtn.isDisplayed();
		return display;
	}
	
	public boolean recruitersDirectory() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement rd = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 16, 1)));
		rd.click();
		WebElement searchBox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",17, 1)));
		searchBox.sendKeys("John");
		Thread.sleep(2000);
		searchBox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		WebElement populatedData = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",18, 1)));
		Thread.sleep(2000);
		return populatedData.isDisplayed();
	}
	
	public boolean employerDirectory() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement ed = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",19, 1)));
		ed.click();
		WebElement searchBox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 17, 1)));
		searchBox.sendKeys("Stanford");
		Thread.sleep(2000);
		searchBox.sendKeys(Keys.ENTER);
		Thread.sleep(2000);
		WebElement populatedData = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",18, 1)));
		Thread.sleep(2000);
		return populatedData.isDisplayed();
	}
	
	public boolean aboutUsBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement aboutUsBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 20, 1)));
		aboutUsBtn.click();
		Thread.sleep(2000);
        WebElement aboutUsData = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",21, 1)));
		return aboutUsData.isDisplayed();
	}
	
	public void faqsBtn() throws EncryptedDocumentException, IOException
	{
		WebElement faqsBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",22, 1)));
		act.moveToElement(faqsBtn);
		
	}
	
	public String contactUs() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement contactUs = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",23, 1)));
		contactUs.click();
		Thread.sleep(2000);
		WebElement contactUsLabel = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",24, 1)));
		return contactUs.getText();
	}
	
	public String dashboardbtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement dashboardBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 25, 1)));
		dashboardBtn.click();
		Thread.sleep(2000);
		WebElement hbiDashboardData = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",26, 1)));
		return hbiDashboardData.getText();
	}
	
	public String grievanceBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement grievanceBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",27, 1)));
		grievanceBtn.click();
		Thread.sleep(2000);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();
	}
	
	public boolean hpSearchBox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement searchTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",29, 1)));
		js.executeScript("window.scrollBy(0,300)", "");
//		js.executeScript("arguments[0].scrollIntoView();", searchTextbox);
		Thread.sleep(2000);
     	searchTextbox.sendKeys("madhura");
		Thread.sleep(1000);
		WebElement searchBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",30, 1)));
		searchBtn.click();
		Thread.sleep(2000);
		WebElement searchedData = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",31, 1)));
		return searchedData.isDisplayed();
	}
	
	public String mohfwLinkAtFooter() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement mohfwLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",34, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", mohfwLink);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();		
	}
	
	public String ministryOfExternalAffairsLinkAtFooter() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement moeaLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",35, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", moeaLink);
		String parentWindow    = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr   = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();
	}	
	
	public String consularServiceManagementSystemLinkAtFooter() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement csmsLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",36, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", csmsLink);
		String parentWindow    = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr   = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();		
	}
	
	public String emigrationLinkAtFooter() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement emigrationLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 37, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", emigrationLink);
		String parentWindow    = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr   = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();
	}
	
	public String cpgramsHomeLinkAtFooter() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement cpgramsHomeLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 38, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", cpgramsHomeLink);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();		
	}
	
	public String youTubeLink() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement mohfwYouTubeLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage",39, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();", mohfwYouTubeLink);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();		
	}
	
	   public String twitterLink() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement mohfwTwitterLink = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 40, 1)));
		Thread.sleep(2000);
		js.executeScript("arguments[0].click();",mohfwTwitterLink);
		String parentWindow = driver.getWindowHandle();
		Set<String> allWindows = driver.getWindowHandles();
		Iterator<String> itr = allWindows.iterator();
		while(itr.hasNext())
		{
			String childWindow = itr.next();

			if(!parentWindow.equals(childWindow))
			{
			   driver.switchTo().window(childWindow);
			   Thread.sleep(2000);
            }
     	}
		return driver.getCurrentUrl();		
	}
	   
	   public boolean facebookLogo() throws EncryptedDocumentException, IOException 
	   {
		   WebElement facebookLogo = driver.findElement(By.xpath(ReadExcel.readExcelFile("HBI_HomePage", 41, 1)));
		   js.executeScript("arguments[0].scrollIntoView();",facebookLogo );
		   return facebookLogo.isEnabled();
		   
	   }
}
