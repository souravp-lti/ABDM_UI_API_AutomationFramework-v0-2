package HBI;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class Demo_Reg_NO extends MainBaseFeature
{
	JavascriptExecutor js = (JavascriptExecutor)driver;
	Actions act =  new Actions(driver);

	  public void reg_RA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		
		WebElement loginRegisterBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 2, 1)));
	    loginRegisterBtn.click();
		WebElement recruitementAgentOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 18, 1)));
        recruitementAgentOpt.click();
		WebElement registerButton = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 4, 1)));
        registerButton.click();
        Thread.sleep(1000);
                
		WebElement enterRegistrationNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 19, 1)));
		enterRegistrationNumberTextbox.sendKeys("AN1234567");
		WebElement nameOfRecruitingAgencyTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 1)));
		nameOfRecruitingAgencyTextbox.sendKeys("LTI");
		WebElement meaRegisteredContactNumber = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 21, 1)));
		meaRegisteredContactNumber.sendKeys("12345678790");	
		WebElement enterMEARegisteredEmailID = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 22, 1)));
		enterMEARegisteredEmailID.sendKeys("hbhjewb@gmail.com");
		
//Calender
		WebElement registrationValidUptoCalender = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 23, 1)));
		registrationValidUptoCalender.click();
//		Thread.sleep(1000);
		WebElement date = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 26, 1)));
		date.click();
//		Thread.sleep(1000);
		
//Select Country recruiting for listbox
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement selectCountryRecruitingForListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 27, 1)));
		selectCountryRecruitingForListbox.click();
//      js.executeScript("arguments[0].click();", selectCountryRecruitingForListbox);
       
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCountries = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 28, 3)));
        for(int i=0; i<listOfAllTheCountries.size(); i++ )
        {
        	String need = "India";
        	String actual = listOfAllTheCountries.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheCountries.get(i).click();
        		Thread.sleep(1000);
        		act.sendKeys(Keys.ESCAPE).build().perform();
        		Thread.sleep(1000);
        		break;
        	}
        }
                
  /*      
        WebElement searchBox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 28, 1)));
		searchBox.sendKeys("India");
        Thread.sleep(1000);
        WebElement selectIndiaOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 29, 1)));
        js.executeScript("arguments[0].click();", selectIndiaOpt);
//      selectIndiaOpt.click();
        Thread.sleep(1000);        */
        
//Preferred Healthcare Professional selection listbox
		WebElement recruitingHealthcareProfessionalType = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 30, 1)));
//		js.executeScript( "arguments[0].click();", recruitingHealthcareProfessionalType);
		recruitingHealthcareProfessionalType.click();
        Thread.sleep(1000);
		recruitingHealthcareProfessionalType.click();
		Thread.sleep(1000);
        List<WebElement>  listOfAllHPTypes = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 30, 3)));
        for(int i=0; i<listOfAllHPTypes.size(); i++ )
        {
        	String need = "Optometrist";
        	String actual = listOfAllHPTypes.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllHPTypes.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
		
/*	
        WebElement searchboxHP = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 31, 1)));
        searchboxHP.sendKeys("Optometrist");
        Thread.sleep(1000);
		WebElement optometristOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 32, 1)));
        optometristOpt.click();
        Thread.sleep(1000);    */
		WebElement alreadySelectedCountry = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 33, 1)));
		js.executeScript( "arguments[0].click();", alreadySelectedCountry);
		
//State Selection listbox
		WebElement stateSelectionListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 34, 1)));
		js.executeScript( "arguments[0].click();", stateSelectionListbox);
//		stateSelectionListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheStates = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 35, 1)));
        for(int i=1; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "MAHARASHTRA";
        	String actual = listOfAllTheStates.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
        
//City/Town/Village Listbox
		WebElement citySelectListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 36, 1)));
		js.executeScript( "arguments[0].click();", citySelectListbox);
//		citySelectListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCities = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 37, 1)));
        for(int i=0; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "NAGPUR";
        	String actual = listOfAllTheCities.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
		
		//Address textbox
		WebElement addressTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 38, 1)));
        addressTextbox.sendKeys("Morya Park Lane Number 5, Kai Arunatai Skharam Devkar Nagar, Pimple Gurav, Pimpri-Chinchwad, Pune");
        Thread.sleep(1000);
        
        //Pin code
		WebElement pinCodeTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 39, 1)));
        pinCodeTextbox.sendKeys("416016");
        Thread.sleep(1000);

        //Create Password
		WebElement createPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 40, 1)));
        createPassword.sendKeys("Recruiter@123");
        Thread.sleep(1000);

      //Confirm Password
      	WebElement confirmPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 41, 1)));
        createPassword.sendKeys("Recruiter@123");
        Thread.sleep(1000);
		
       //Update and Submit button
      	WebElement updateAndSubmitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 42, 1)));
        updateAndSubmitBtn.click();
   
    
    
	}

}
