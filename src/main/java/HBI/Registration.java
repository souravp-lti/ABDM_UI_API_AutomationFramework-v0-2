package HBI;

import java.io.IOException;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import Base.MainBaseFeature;
import Utility.ReadExcel;

public class Registration extends MainBaseFeature
{ 
	JavascriptExecutor js = (JavascriptExecutor)driver;
	Actions act =  new Actions(driver);
	
	public void landingOnRegistartionPageOfRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		WebElement loginRegisterBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 2, 1)));
	    loginRegisterBtn.click();
		WebElement recruitementAgentOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 18, 1)));
        recruitementAgentOpt.click();
		WebElement registerButton = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 4, 1)));
        registerButton.click();
        Thread.sleep(1000);
	}
	
	public void landingOnRegistrationPageOfNO() throws InterruptedException, EncryptedDocumentException, IOException
	{
		WebElement loginRegisterBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 2, 1)));
	    loginRegisterBtn.click();
	    Thread.sleep(1000);
		WebElement nodalOfficerOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 3, 1)));
        nodalOfficerOpt.click();
        Thread.sleep(1000);
		WebElement registerButton = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 4, 1)));
        registerButton.click();
        Thread.sleep(1000);
	}

/* ---------------------Nodal Officer---------------------------
 * ---------------------------------------------------------------------	
 */
	public String nodalOfficerLabel() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistrationPageOfNO();

		WebElement label = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 14, 1)));
        return label.getText();
	}

//Name Of Authorised Official Textbox
	public String nameOfAuthorisedOfficialTextbox() throws InterruptedException, EncryptedDocumentException, IOException
	{
		landingOnRegistrationPageOfNO(); // Landing on 

        WebElement nameOfAuthorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 5, 1)));
        nameOfAuthorisedOfficialTextbox.sendKeys("12234");
        Thread.sleep(1000);
        
        WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 15, 1)));
        if(errorMsge.getText().equalsIgnoreCase("Incorrect input"))
        {
        	nameOfAuthorisedOfficialTextbox.clear();
            Thread.sleep(1000);
            nameOfAuthorisedOfficialTextbox.sendKeys("Anshul Siddham");
            Thread.sleep(1000);
            String value = nameOfAuthorisedOfficialTextbox.getAttribute("value");
            return value;
        }
        else
        {
        	System.out.println("Issue in Authorised Official Textbox");
        }
        return null;
	}
	
    public String authorisedOfficialTextbox() throws InterruptedException, EncryptedDocumentException, IOException
    {

        landingOnRegistrationPageOfNO();
        
        WebElement authorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 6, 1)));
        authorisedOfficialTextbox.sendKeys("12234");
        Thread.sleep(1000);
        
        WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 15, 1)));
        if(errorMsge.getText().equalsIgnoreCase("Incorrect input"))
        {
        	authorisedOfficialTextbox.clear();
            Thread.sleep(1000);
            authorisedOfficialTextbox.sendKeys("Healthcare");
            Thread.sleep(1000);
            String value = authorisedOfficialTextbox.getAttribute("value");
            return value;
        }
        else
        {
        	System.out.println("Issue in Authorised Official Textbox");
        }
        return null;
               
    }
    
//Designation textbox 
    public String designation() throws InterruptedException, EncryptedDocumentException, IOException
    {
        landingOnRegistrationPageOfNO();

        WebElement designation = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 7, 1)));
        designation.sendKeys("12234");
        Thread.sleep(1000);
        
        WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 15, 1)));
        if(errorMsge.getText().equalsIgnoreCase("Incorrect input"))
        {
        	designation.clear();
            Thread.sleep(1000);
            designation.sendKeys("Nodal Officer");
            Thread.sleep(1000);
            String value = designation.getAttribute("value");
            return value;
        }
        else
        {
        	System.out.println("Issue in Designation Textbox");
        }
        return null;
    }
 
//Create Password textbox    
    public String createPasswordTextbox() throws InterruptedException, EncryptedDocumentException, IOException
    {
        landingOnRegistrationPageOfNO();
 
        js.executeScript("window.scrollBy(0,300)", "");
        WebElement createPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 10, 1)));
        createPassword.sendKeys("anshul123");
        Thread.sleep(1000);
        
        WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 15, 1)));
        if(errorMsge.isDisplayed())
        {
        	createPassword.clear();
            Thread.sleep(1000);
            createPassword.sendKeys("Dv@123456");
            Thread.sleep(1000);
            String value = createPassword.getAttribute("value");
            return value;
        }
        else
        {
        	System.out.println("Issue in Create Password Textbox");
        }
        return null;
               
    }
	
//Confirm Password textbox    
    public String confirmPasswordTextbox() throws InterruptedException, EncryptedDocumentException, IOException
    {
        landingOnRegistrationPageOfNO();

        js.executeScript("window.scrollBy(0,300)", "");
        WebElement confirmPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 11, 2)));
        confirmPassword.sendKeys("anshul123");
        Thread.sleep(1000);
        
        	confirmPassword.clear();
            Thread.sleep(1000);
            confirmPassword.sendKeys("Dv@123456");
            Thread.sleep(1000);
            String value = confirmPassword.getAttribute("value");
            return value;
            
    }
 
//Contact Number textbox    
    public String contactNumberTextbox() throws InterruptedException, EncryptedDocumentException, IOException
    {

        landingOnRegistrationPageOfNO();

        WebElement officialContactNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 8, 1)));
        officialContactNumberTextbox.sendKeys("1234567890");
        WebElement getOtpBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 16, 1)));
        getOtpBtn.click();
        Thread.sleep(2000);
        
        WebElement enterOtpDialogueboxHeading = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 16, 3)));
        return enterOtpDialogueboxHeading.getText();
    	
    }

//Email ID textbox 
    public String emailIDTextbox() throws InterruptedException, EncryptedDocumentException, IOException
    {

        landingOnRegistrationPageOfNO();

        WebElement emailIDtextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 9, 1)));
        emailIDtextbox.sendKeys("anshulsiddham");
        WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 15, 1)));
        if(errorMsge.getText().equalsIgnoreCase("Incorrect input"))
        {
        	emailIDtextbox.clear();
            Thread.sleep(1000);
            emailIDtextbox.sendKeys("ansh03123@gmail.com");
            Thread.sleep(1000);
            WebElement getOtpBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 17, 1)));
            getOtpBtn.click();
            Thread.sleep(2000);
            
            WebElement enterOtpDialogueboxHeading = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 17, 3)));
            return enterOtpDialogueboxHeading.getText();
        }
        else
        {
        	System.out.println("Issue in Email ID textbox or Get otp button");
        }
        return null;      
    	
    }
		
	
//Complete Registration flow	
	public String registrationOfNO() throws EncryptedDocumentException, IOException, InterruptedException
	{
        landingOnRegistrationPageOfNO();
        
        WebElement nameOfAuthorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 5, 1)));
        nameOfAuthorisedOfficialTextbox.sendKeys("Anshul Siddham");
//        Thread.sleep(1000);
        WebElement authorisedOfficialTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 6, 1)));
        authorisedOfficialTextbox.sendKeys("Healthcare");
//        Thread.sleep(1000);
        WebElement designation = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 7, 1)));
        designation.sendKeys("Nodal Officer");
//        Thread.sleep(1000);
        WebElement officialContactNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 8, 1)));
        officialContactNumberTextbox.sendKeys("7011872405");
        WebElement emailIDtextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 9, 1)));
        emailIDtextbox.sendKeys("sathyamevjaye@gmail.com");
 //       Thread.sleep(1000);
        WebElement createPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 10, 1)));
        createPassword.sendKeys("Nodal@12345");
        WebElement confirmPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 11, 1)));
        confirmPassword.sendKeys("Nodal@12345");
//        Thread.sleep(2000);
        WebElement submitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 12, 1)));
        js.executeScript("arguments[0].scrollIntoView()", submitBtn);
        js.executeScript("arguments[0].click();", submitBtn);
        Thread.sleep(3000);
        
        String alert = driver.switchTo().alert().getText();
        System.out.println("Text = " +alert);
        WebElement congratulationsMessage = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 13, 0)));
        String actualValue = congratulationsMessage.getText();
        
        return actualValue;
	}
	
/* ---------------------Recrutement Agent---------------------
	 * ---------------------------------------------------------------------	
	 */	
	
	public String recruitementAgentLabel() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		WebElement label = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 43, 1)));
		return label.getText();
				
	}
	
	public String enterRegistrationNumberTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		WebElement enterRegistrationNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 19, 1)));
        enterRegistrationNumberTextbox.sendKeys("A12345");
		Thread.sleep(1000);
		WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 19, 3)));
		
		if(errorMsge.getText().equals("Please enter 2 letters followed by 7 digits."))
		{
			enterRegistrationNumberTextbox.clear();
			enterRegistrationNumberTextbox.sendKeys("An123456");
			Thread.sleep(1000);
			return enterRegistrationNumberTextbox.getAttribute("value");
		}
		else
		{
			System.out.println("Issue in Enter Registration Number Textbox");
		}
		
		return null; 	
       
	}
	
	public String nameOfRecruitingAgencyTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		WebElement nameOfRecruitingAgencyTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 1)));
		nameOfRecruitingAgencyTextbox.sendKeys("A12345");
		Thread.sleep(1000);
		WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 3)));
		
		if(errorMsge.getText().equals("Incorrect input"))
		{
			nameOfRecruitingAgencyTextbox.clear();
			nameOfRecruitingAgencyTextbox.sendKeys("Sunpharma Care");
			Thread.sleep(1000);
			return nameOfRecruitingAgencyTextbox.getAttribute("value");
		}
		else
		{
			System.out.println("Issue in Name Of Recruiting Agency Textbox");
		}
		
		return null; 	
       
	}

//Contact Number textbox and send otp button	
	public String contactNumberTextboxAndGetOtpBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		WebElement contactNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 21, 1)));
		contactNumberTextbox.sendKeys("12345678");
		Thread.sleep(1000);
		WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 3)));
		
		if(errorMsge.getText().equals("Incorrect input"))
		{
			contactNumberTextbox.clear();
			contactNumberTextbox.sendKeys("1234567890");
			Thread.sleep(1000);
			WebElement getOtpBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 44, 1)));
			getOtpBtn.click();
			Thread.sleep(1000);
	        WebElement enterOtpDialogueboxHeading = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 16, 3)));
            
			return enterOtpDialogueboxHeading.getText();
		}
		else
		{
			System.out.println("Issue in Contact number Textbox or get otp button");
		}
		
		return null; 	
       
	}

//Email if textbox and get otp button 	
	public String emailIDTextboxAndGetOtpBtn() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		WebElement emailIDTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 22, 1)));
		emailIDTextbox.sendKeys("ansh");
		Thread.sleep(1000);
		WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 3)));
		
		if(errorMsge.getText().equals("Incorrect input"))
		{
			emailIDTextbox.clear();
			emailIDTextbox.sendKeys("ansh03123@gmail.com");
			Thread.sleep(1000);
			WebElement getOtpBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 45, 1)));
			getOtpBtn.click();
			Thread.sleep(1000);
	        WebElement enterOtpDialogueboxHeading = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 17, 3)));
            
			return enterOtpDialogueboxHeading.getText();
		}
		else
		{
			System.out.println("Issue in Email ID Textbox or get otp button");
		}
		
		return null; 	
       
	}
	
	public String registrationValidUptoDropdown() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		js.executeScript("window.scrollBy(0,250)", "");
		WebElement registrationValidUptoCalender = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 23, 1)));
		registrationValidUptoCalender.click();
		Thread.sleep(1000);
		WebElement date = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 26, 3)));
		date.click();
		Thread.sleep(1000);		
		return registrationValidUptoCalender.getAttribute("value");
	     
	}
	
//Select the country recruiting for listbox	
	public String selectTheCountryRecruitingForListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement selectCountryRecruitingForListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 27, 1)));
		selectCountryRecruitingForListbox.click();
//      js.executeScript("arguments[0].click();", selectCountryRecruitingForListbox);
       
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCountries = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 28, 3)));
        for(int i=0; i<listOfAllTheCountries.size(); i++ )
        {
        	String need = "India";
        	String actual = listOfAllTheCountries.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheCountries.get(i).click();
        		Thread.sleep(2000);
        		act.sendKeys(Keys.ESCAPE).build().perform();
        		Thread.sleep(1000);
        		break;
        	}
        }
		WebElement countrySelected = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 27, 3)));

        return countrySelected.getAttribute("value");
	}
	
	public String selectRecruitingHealthcareProfessionalTypeListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement recruitingHealthcareProfessionalType = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 30, 1)));
//		js.executeScript( "arguments[0].click();", recruitingHealthcareProfessionalType);
		recruitingHealthcareProfessionalType.click();
        Thread.sleep(1000);
       
        List<WebElement>  listOfAllHPTypes = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 30, 3)));
        for(int i=0; i<listOfAllHPTypes.size(); i++ )
        {
        	String need = "Optometrist";
        	String actual = listOfAllHPTypes.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllHPTypes.get(i).click();
        		Thread.sleep(2000);
        		act.sendKeys(Keys.ESCAPE).build().perform();
        		Thread.sleep(1000);
        		break;
        	}
        }
		WebElement hpSelected = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 29, 3)));

        return hpSelected.getAttribute("value");
	}
	
//Country Textbox
	public String countryTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
		
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement alreadySelectedCountry = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 33, 1)));
        return alreadySelectedCountry.getAttribute("value");
		
	}

//State Listbox	
	public String stateListybox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
        
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement stateSelectionListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 34, 1)));
//		js.executeScript( "arguments[0].click();", stateSelectionListbox);
		stateSelectionListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheStates = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 35, 1)));
        for(int i=1; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "MAHARASHTRA";
        	String actual = listOfAllTheStates.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(1000);
        		break;
        	}
        }
		WebElement stateSelected = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 34, 3)));
        return stateSelected.getAttribute("value");
		
		
	}

//City Listbox
	public String cityListbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
        
		js.executeScript("window.scrollBy(0, 400);", "");
		
		//To select the state 
		WebElement stateSelectionListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 34, 1)));
//		js.executeScript( "arguments[0].click();", stateSelectionListbox);
		stateSelectionListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheStates = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 35, 1)));
        for(int i=1; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "MAHARASHTRA";
        	String actual = listOfAllTheStates.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(1000);
        		break;
        	}
        }
        //To select the city
		WebElement citySelectListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 36, 1)));
//		js.executeScript( "arguments[0].click();", stateSelectionListbox);
		citySelectListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCities = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 37, 1)));
        for(int i=1; i<=listOfAllTheCities.size(); i++ )
        {
        	String need = "NAGPUR";
        	String actual = listOfAllTheCities.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheCities.get(i).click();
        		Thread.sleep(1000);
        		break;
        	}
        }
		WebElement citySelected = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 36, 3)));
        return citySelected.getAttribute("value");
		
		
	}
	
//Address Textbox
	public boolean addresstextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
        landingOnRegistartionPageOfRA();
        
		js.executeScript("window.scrollBy(0, 500);", "");
		WebElement addressTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 38, 1)));
        return addressTextbox.isDisplayed();
	}
	
//Pincode textbox
	public String pincodeTextbox() throws EncryptedDocumentException, IOException, InterruptedException
	{
        landingOnRegistartionPageOfRA();
        
		js.executeScript("window.scrollBy(0, 500);", "");
		WebElement pinCodeTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 39, 1)));
		pinCodeTextbox.sendKeys("1234"); 
		Thread.sleep(1000);
		WebElement errorMsge = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 39, 3)));
		
		if(errorMsge.getText().equals("Please enter 6 digit pincode"))
	    {
	    	pinCodeTextbox.clear();
	    	Thread.sleep(1000);
	    	pinCodeTextbox.sendKeys("123456");
	    	Thread.sleep(1000);
	    	return pinCodeTextbox.getAttribute("value");
	    }
	    else 
	    {
	    	System.out.println("Issue in Pincode Textbox");
	    }
	    return null;
	}
	
	
//Full flow of Registration of RA	
	public String registrationOfRA() throws EncryptedDocumentException, IOException, InterruptedException
	{
		landingOnRegistartionPageOfRA();
                
		WebElement enterRegistrationNumberTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 19, 1)));
		enterRegistrationNumberTextbox.sendKeys("AN1234567");
		WebElement nameOfRecruitingAgencyTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 20, 1)));
		nameOfRecruitingAgencyTextbox.sendKeys("LTI");
		WebElement meaRegisteredContactNumber = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 21, 1)));
		meaRegisteredContactNumber.sendKeys("12345678790");	
		WebElement enterMEARegisteredEmailID = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 22, 1)));
		enterMEARegisteredEmailID.sendKeys("hbhjewb@gmail.com");
		
//Calender
		WebElement registrationValidUptoCalender = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 23, 1)));
		registrationValidUptoCalender.click();
//		Thread.sleep(1000);
		WebElement date = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 26, 1)));
		date.click();
//		Thread.sleep(1000);
		
//Select Country recruiting for listbox
		js.executeScript("window.scrollBy(0, 400);", "");
		WebElement selectCountryRecruitingForListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 27, 1)));
		selectCountryRecruitingForListbox.click();
//      js.executeScript("arguments[0].click();", selectCountryRecruitingForListbox);
       
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCountries = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 28, 3)));
        for(int i=0; i<listOfAllTheCountries.size(); i++ )
        {
        	String need = "India";
        	String actual = listOfAllTheCountries.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheCountries.get(i).click();
        		Thread.sleep(1000);
        		act.sendKeys(Keys.ESCAPE).build().perform();
        		Thread.sleep(1000);
        		break;
        	}
        }
                
  /*      
        WebElement searchBox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 28, 1)));
		searchBox.sendKeys("India");
        Thread.sleep(1000);
        WebElement selectIndiaOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 29, 1)));
        js.executeScript("arguments[0].click();", selectIndiaOpt);
//      selectIndiaOpt.click();
        Thread.sleep(1000);        */
        
//Preferred Healthcare Professional selection listbox
		WebElement recruitingHealthcareProfessionalType = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 30, 1)));
//		js.executeScript( "arguments[0].click();", recruitingHealthcareProfessionalType);
		recruitingHealthcareProfessionalType.click();
        Thread.sleep(1000);
		recruitingHealthcareProfessionalType.click();
		Thread.sleep(1000);
        List<WebElement>  listOfAllHPTypes = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 30, 3)));
        for(int i=0; i<listOfAllHPTypes.size(); i++ )
        {
        	String need = "Optometrist";
        	String actual = listOfAllHPTypes.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllHPTypes.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
		
/*	
        WebElement searchboxHP = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 31, 1)));
        searchboxHP.sendKeys("Optometrist");
        Thread.sleep(1000);
		WebElement optometristOpt = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 32, 1)));
        optometristOpt.click();
        Thread.sleep(1000);    */
		WebElement alreadySelectedCountry = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 33, 1)));
		js.executeScript( "arguments[0].click();", alreadySelectedCountry);
		
//State Selection listbox
		WebElement stateSelectionListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 34, 1)));
		js.executeScript( "arguments[0].click();", stateSelectionListbox);
//		stateSelectionListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheStates = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 35, 1)));
        for(int i=1; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "MAHARASHTRA";
        	String actual = listOfAllTheStates.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
        
//City/Town/Village Listbox
		WebElement citySelectListbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 36, 1)));
		js.executeScript( "arguments[0].click();", citySelectListbox);
//		citySelectListbox.click();
        Thread.sleep(1000);
        List<WebElement>  listOfAllTheCities = driver.findElements(By.xpath(ReadExcel.readExcelFile("Registration", 37, 1)));
        for(int i=0; i<=listOfAllTheStates.size(); i++ )
        {
        	String need = "NAGPUR";
        	String actual = listOfAllTheCities.get(i).getText();
        	if(need.equals(actual))
        	{
        		listOfAllTheStates.get(i).click();
        		Thread.sleep(2000);
        		break;
        	}
        }
		
		//Address textbox
		WebElement addressTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 38, 1)));
        addressTextbox.sendKeys("Morya Park Lane Number 5, Kai Arunatai Skharam Devkar Nagar, Pimple Gurav, Pimpri-Chinchwad, Pune");
        Thread.sleep(1000);
        
        //Pin code
		WebElement pinCodeTextbox = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 39, 1)));
        pinCodeTextbox.sendKeys("416016");
        Thread.sleep(1000);

        //Create Password
		WebElement createPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 40, 1)));
        createPassword.sendKeys("Recruiter@123");
        Thread.sleep(1000);

      //Confirm Password
      	WebElement confirmPassword = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 41, 1)));
        createPassword.sendKeys("Recruiter@123");
        Thread.sleep(1000);
		
       //Update and Submit button
      	WebElement updateAndSubmitBtn = driver.findElement(By.xpath(ReadExcel.readExcelFile("Registration", 42, 1)));

      	return null;
       	
   }


}
